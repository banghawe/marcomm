﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Marcomm.Web.Startup))]
namespace Marcomm.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
