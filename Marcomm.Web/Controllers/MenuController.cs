﻿using Marcomm.DAL;
using Marcomm.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Marcomm.Web.Controllers
{
    public class MenuController : Controller
    {
        // GET: Menu
        public ActionResult Index()
        {
            return View(MenuDAL.Get());
        }

        public ActionResult List()
        {
            return PartialView(MenuDAL.Get());
        }

        public ActionResult Add()
        {
            ViewBag.ListMenu = new SelectList(MenuDAL.Get(), "Id", "name");
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(MenuViewModel model)
        {
            model.Code = MenuDAL.CodeGenerator();
            if (ModelState.IsValid)
            {
                if (MenuDAL.CheckIsDelete(model)!=null)
                {
                    if (MenuDAL.Update(MenuDAL.CheckIsDelete(model)))
                    {
                        var result = new { success = true, message = "Done", action = "edit", code = model.Code };
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var result = new { success = true, message = "false", action = "edit", code = model.Code };
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                }
                else if (MenuDAL.CheckName(model.name))
                {
                    if (MenuDAL.Insert(model))
                    {
                        var result = new { success = true, message = "Done", action = "add", code = model.Code };
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var result = new { success = false, message = "Failed" };
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    var result = new { success = false, message = "The menu " + model.name + " is already exist", exist = true };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            return PartialView(model);
        }

        public ActionResult Edit(int id)
        {
            return PartialView(MenuDAL.Get(id));
        }

        [HttpPost]
        public ActionResult Edit(MenuViewModel model)
        {
            if (MenuDAL.Update(model))
            {
                var result = new { success = true, message = "Done", action="edit" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = new { success = false, message = "Failed" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView();
        }

        public ActionResult Delete(int id)
        {
            return PartialView(MenuDAL.Get(id));
        }

        [HttpPost]
        public ActionResult Delete(MenuViewModel model)
        {
            if (MenuDAL.Delete(model))
            {
                var result = new { success = true, message = "Done", action = "delete", code = model.Code };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = new { success = false, message = "Failed" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView();
        }

        public ActionResult Detail(int id)
        {
            return PartialView(MenuDAL.Get(id));
        }

    }
}