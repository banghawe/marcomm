﻿using Marcomm.DAL;
using Marcomm.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Marcomm.Web.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Employee
        public ActionResult Index()
        {
            ViewBag.ListCompanyName = new SelectList(CompanyDAL.Get(), "name", "name");
            return View(EmployeeDAL.Get());
        }

        public ActionResult List()
        {
            return PartialView(EmployeeDAL.Get());
        }

        public ActionResult Add()
        {
            EmployeeViewModel employee = new EmployeeViewModel();
            ViewBag.Company = new SelectList(CompanyDAL.Get(), "id", "name");

            return PartialView(employee);
        }

        [HttpPost]
        public ActionResult Add(EmployeeViewModel model)
        {
            ViewBag.Company = new SelectList(CompanyDAL.Get(), "id", "name");
            if (ModelState.IsValid)
            {
                model.isDelete = false;
                model.createdBy = User.Identity.Name;
                model.createdDate = DateTime.Now.Date;
                if (EmployeeDAL.Insert(model))
                {
                    var result = new { success = true, message = "Data Berhasil Tersimpan" };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var result = new { success = false, message = "Data Gagal Tersimpan" };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            return PartialView(model);
        }

        public ActionResult Edit(int Id)
        {
            ViewBag.Company = new SelectList(CompanyDAL.Get(), "id", "name");
            return PartialView(EmployeeDAL.Get(Id));
        }

        [HttpPost]
        public ActionResult Edit(EmployeeViewModel model)
        {
            ViewBag.Company = new SelectList(CompanyDAL.Get(), "id", "name");
            if (ModelState.IsValid)
            {
                if (EmployeeDAL.Update(model))
                {
                    var result = new { success = true, message = "Data Berhasil diubah" };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var result = new { success = false, message = "Data Gagal diubah" };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }


            return PartialView(model);
        }

        public ActionResult Delete(int id)
        {
            var data = EmployeeDAL.Get(id);
            EmployeeDAL.Delete(id);
            var result = new
            {
                success = true,
                tipe = "info",
                pesan = "data deleted",
                alert = "data company with employee id number" + data.code + "has been deleted"
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public ActionResult Delete(EmployeeViewModel model)
        //{
        //    if (EmployeeDAL.Delete(model))
        //    {
        //        var result = new { success = true, message = "Data Berhasil Dihapus" };
        //        return Json(result, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        var result = new { success = false, message = "Data Gagal Dihapus" };
        //        return Json(result, JsonRequestBehavior.AllowGet);
        //    }

        //    return PartialView();
        //}

        public ActionResult Details(int id)
        {
            return PartialView(EmployeeDAL.Get(id));
        }

        public JsonResult nama(string nama, int id)
        {
            var result = new
            {
                data = EmployeeDAL.NomorEmployee(nama, id),
                success = true
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult panggil(int id)
        {
            var result = new
            {
                data = EmployeeDAL.Get(id),
                success = true
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}