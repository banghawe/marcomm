﻿using Marcomm.ViewModel;
using Marcomm.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Marcomm.Web.Controllers
{
    [Authorize]
    public class EventController : Controller
    {
        public ActionResult Index()
        {
            return View(EventDAL.Get());
        }

        public ActionResult List()
        {
            return PartialView(EventDAL.Get());
        }

        /*[HttpPost]
        public ActionResult Search(EventViewModel model)
        {
            return View("Index",EventDAL.Get(model));
        }*/

        public ActionResult Add()
        {
            EventViewModel model = new EventViewModel();
            model.code = EventDAL.getCode();
            return PartialView(model);
        }

        [HttpPost]
        public ActionResult Add(EventViewModel model)
        {
            var User = (Marcomm.ViewModel.UserViewModel)Session["User"];
            model.createdBy = User.mEmployeeName;
            model.requestBy = User.mEmployeeId;
            model.status = 1;
            model.request_date = DateTime.Now.Date;
            model.isDelete = false;
            model.createdDate = DateTime.Now.Date;
            if (EventDAL.chkDate(model.startDate,model.endDate) == true)
            {
                if (ModelState.IsValid)
                {
                    if (EventDAL.Insert(model))
                    {
                        var result = new { success = true, message = "Data Berhasil Tersimpan", code = model.code, from = "add" };
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var result = new { success = false, message = "Data Gagal Tersimpan" };
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                }
            }else
            {
                var result = new { success = "date", message = "Data Gagal Tersimpan" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            
            return PartialView(model);
        }

        public ActionResult Approve(int id)
        {
            ViewBag.ListAssign = new SelectList(UserDAL.GetUser(), "mEmployeeId", "mEmployeeName");
            return PartialView(EventDAL.Get(id));
        }

        [HttpPost]
        public ActionResult Approve(EventViewModel model)
        {
            var User = (Marcomm.ViewModel.UserViewModel)Session["User"];
            model.approvedBy = User.mEmployeeId;
            model.approvedDate = DateTime.Now.Date;
            model.status = 2;
            model.updateDate = DateTime.Now.Date;
            model.updateBy = User.mEmployeeName;
            if (EventDAL.UpdateAppr(model))
            {
                var result = new { success = true, message = "Data Berhasil diubah", code = model.code, from = "approve" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = new { success = false, message = "Data Gagal diubah" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView();
        }

        public ActionResult Close(int id)
        {
            ViewBag.ListAssign = new SelectList(UserDAL.GetUser(), "mEmployeeId", "mEmployeeName");
            return PartialView(EventDAL.Get(id));
        }

        [HttpPost]
        public ActionResult Close(EventViewModel model)
        {
            model.closeDate = DateTime.Now.Date;
            model.status = 3;
            if (EventDAL.UpdateClose(model))
            {
                var result = new { success = true, message = "Data Berhasil diubah", code = model.code, from = "close" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = new { success = false, message = "Data Gagal diubah" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView();
        }

        public ActionResult Edit(int id)
        {
            return PartialView(EventDAL.Get(id));
        }

        [HttpPost]
        public ActionResult Edit(EventViewModel model)
        {
            var User = (Marcomm.ViewModel.UserViewModel)Session["User"];
            model.updateBy = User.mEmployeeName;
            model.updateDate = DateTime.Now.Date;
            if (EventDAL.Update(model))
            {
                var result = new { success = true, message = "Data Berhasil diubah", code = model.code, from = "edit" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = new { success = false, message = "Data Gagal diubah" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView();
        }

        public ActionResult Delete(int id)
        {
            return PartialView(EventDAL.Get(id));
        }

        [HttpPost]
        public ActionResult Delete(EventViewModel model)
        {
            if (EventDAL.Delete(model))
            {
                var result = new { success = true, message = "Data Berhasil Dihapus", from = "delete", code = model.code };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = new { success = false, message = "Data Gagal Dihapus" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            return PartialView();
        }

        public ActionResult Details(int id)
        {
            ViewBag.ListAssign = new SelectList(UserDAL.GetUser(), "mEmployeeId", "mEmployeeName");
            return PartialView(EventDAL.Get(id));
        }

        public ActionResult Rejected(int id)
        {
            return PartialView(EventDAL.Get(id));
        }

        [HttpPost]
        public ActionResult Rejected(EventViewModel model)
        {
            model.status = 0;
            if (EventDAL.UpdateRjt(model))
            {
                var result = new { success = true, message = "Data Berhasil diubah", code = model.code, from = "reject" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = new { success = false, message = "Data Gagal diubah" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView();
        }
    }
}