﻿using Marcomm.DAL;
using Marcomm.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Marcomm.Web.Controllers
{
    public class CompanyController : Controller
    {
        // GET: Company
        public ActionResult Index()
        {
            ViewBag.ListCompanyCode = new SelectList(CompanyDAL.Get(), "code", "code");
            ViewBag.ListCompanyName = new SelectList(CompanyDAL.Get(), "name", "name");
            return View(CompanyDAL.Get());
        }

        public ActionResult List()
        {
            return PartialView(CompanyDAL.Get());
        }

        public ActionResult Add()
        {
            var data = new CompanyViewModel();
            data.code = CompanyDAL.KodeAuto();
            return PartialView(data);
        }

        [HttpPost]
        public ActionResult Add(CompanyViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (CompanyDAL.Cek(model.name, model.id) == true)
                {
                    if (CompanyDAL.Insert(model) == true)
                    {
                        var result = new
                        {
                            success = true,
                            alertType = "info",
                            alertStrong = "Data Saved!",
                            alertMessage = "New Company has been add with code " + model.code
                        };
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                }
                else if (CompanyDAL.Cek(model.name, model.id) == false)
                {
                    var result = new { success = false };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            return PartialView();
        }

        public ActionResult Edit(int id)
        {
            return PartialView(CompanyDAL.Get(id));
        }

        [HttpPost]
        public ActionResult Edit(CompanyViewModel model)
        {
            //if (ModelState.IsValid)
            {
                if (CompanyDAL.Cek(model.name, model.id) == true)
                {
                    if (CompanyDAL.Update(model) == true)
                    {
                        var result = new 
                        { 
                            success = true, 
                            alertType="info",
                            alertStrong="Data Updated!",
                            alertMessage="New Company has been updated!"
                        };
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                }
                else if (CompanyDAL.Cek(model.name, model.id)==false)
                {
                    var result = new { success = false };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            return PartialView(model);
        }

        public ActionResult Delete(int id)
        {
            var data = CompanyDAL.Get(id);
            CompanyDAL.Delete(id);
            var result = new
            {
                success = true
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public ActionResult Delete(CompanyViewModel model)
        //{
        //    if (CompanyDAL.Delete(model))
        //    {
        //        var result = new { success = true, message = "Success" };
        //        return Json(result, JsonRequestBehavior.AllowGet);
        //    }
        //    else
        //    {
        //        var result = new { success = false, message = "Failed" };
        //        return Json(result, JsonRequestBehavior.AllowGet);
        //    }
        //    return PartialView();
        //}

        public ActionResult Detail(int id)
        {
            return PartialView(CompanyDAL.Get(id));
        }

        public ActionResult panggil(int id)
        {
            var result = new
            {
                success = true,
                data = CompanyDAL.Get(id)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}