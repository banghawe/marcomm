﻿using Marcomm.DAL;
using Marcomm.ViewModel;
using Marcommm.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Marcomm.Web.Controllers
{
    public class MenuAccessController : Controller
    {
        // GET: MenuAccess
        public ActionResult Index()
        {
            return View(RoleDAL.Get());
        }
        public ActionResult List()
        {
            return PartialView(RoleDAL.Get());
        }

        public ActionResult Add()
        {
            ViewBag.ListMenu = new SelectList(MenuDAL.Get(), "Id", "name");
            ViewBag.ListRole = new SelectList(RoleDAL.Get(), "id", "name");
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(MenuAccessViewModel model)
        {
            ViewBag.ListMenu = new SelectList(MenuDAL.Get(), "Id", "name");
            ViewBag.ListRole = new SelectList(RoleDAL.Get(), "id", "name");

            model.createdBy = "Admin";
            model.isDelete = false;
            if (ModelState.IsValid)
            {
                if (MenuAccessDAL.Insert(model))
                {
                    var result = new { success = true, message = "Tambah data" };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var result = new { success = false, message = "Gagal Tambah Data" };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            return PartialView();
        }

        public ActionResult Edit(int id)
        {
            ViewBag.ListMenu = new SelectList(MenuDAL.Get(), "Id", "name");
            ViewBag.ListRole = new SelectList(RoleDAL.Get(), "id", "name");
            return PartialView(MenuAccessDAL.Get(id));
        }

        [HttpPost]
        public ActionResult Edit(MenuAccessViewModel model)
        {
            model.createdBy = "Admin";
            ViewBag.ListMenu = new SelectList(MenuDAL.Get(), "Id", "name");
            ViewBag.ListRole = new SelectList(RoleDAL.Get(), "id", "name");
            if (MenuAccessDAL.Update(model))
            {
                var result = new { success = true, message = "Ubah Data" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = new { success = false, message = "Gagal Ubah data" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView();
        }

        public ActionResult Delete(int id)
        {
            return PartialView(MenuAccessDAL.Get(id));
        }

        [HttpPost]
        public ActionResult Delete(MenuAccessViewModel model)
        {
            if (MenuAccessDAL.Delete(model))
            {
                var result = new { success = true, message = "Done" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = new { success = false, message = "Failed" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Detail(int id)
        {
            ViewBag.ListMenu = new SelectList(MenuDAL.Get(), "Id", "name");
            ViewBag.ListRole = new SelectList(RoleDAL.Get(), "id", "name");
            return PartialView(MenuAccessDAL.Get(id));
        }
        public JsonResult cariId(int idRole)
        {
            var data = new
            {
                success = true,
                item = MenuAccessDAL.getID(idRole)
            };
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}