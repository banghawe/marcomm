﻿using Marcomm.DAL;
using Marcomm.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Marcomm.Web.Controllers
{
    public class LookUpController : Controller
    {
        // GET: LookUp
        public ActionResult Index()
        {
            return View(LookUpDAL.Get());
        }

        public ActionResult List()
        {
            return PartialView(LookUpDAL.Get());
        }

        public ActionResult Add()
        {
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(LookUpViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (LookUpDAL.Insert(model))
                {
                    var result = new { success = true, message = "Data Berhasil Tersimpan", from="add", code=model.name};
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var result = new { success = false, message = "Data Gagal Tersimpan" };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            return PartialView(model);
        }
        public ActionResult Edit(int Id)
        {
            return PartialView(LookUpDAL.Get(Id));
        }

        [HttpPost]
        public ActionResult Edit(LookUpViewModel model)
        {
                if (LookUpDAL.Update(model))
                {
                    var result = new { success = true, message = "Data Berhasil diubah", from = "edit", code = model.name };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var result = new { success = false, message = "Data Gagal diubah" };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            return PartialView(model);
        }

        public ActionResult Delete(int id)
        {
            return PartialView(LookUpDAL.Get(id));
        }

        [HttpPost]
        public ActionResult Delete(LookUpViewModel model)
        {
            if (LookUpDAL.Delete(model))
            {
                var result = new { success = true, message = "Data Berhasil Dihapus", from="delete" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = new { success = false, message = "Data Gagal Dihapus" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            return PartialView();
        }

    }
}