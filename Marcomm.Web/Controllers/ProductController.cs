﻿using Marcomm.DAL;
using Marcomm.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Marcomm.Web.Controllers
{
    public class ProductController : Controller
    {
        // GET: Product
        public ActionResult Index()
        {
            return View(ProductDAL.Get());
        }

        public ActionResult List()
        {
            return PartialView(ProductDAL.Get());
        }

        [HttpGet]
        public ActionResult Add()
        {
            var data = new ProductViewModel();
            data.code = ProductDAL.KodeAuto();
            return PartialView(data);
        }


        [HttpPost]
        public ActionResult Add(ProductViewModel model)
        {

            if (ModelState.IsValid)
            {
                model.isDelete = false;
                model.createdBy = "Admin";//User.Identity.Name;
                model.createdDate = DateTime.Now;
                if (ProductDAL.Koreksi(model.name, model.id) == true)
                {
                    if (ProductDAL.Insert(model) == true)
                    {
                        var result = new
                        {
                            success = true,
                            alertType = "info",
                            alertStrong = "Data Saved",
                            alertMessage = "New Product has been add with " + model.code + " !"
                        };
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }

                }
                else if (ProductDAL.Koreksi(model.name, model.id) == false)
                {
                    var result = new { success = false };

                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            return PartialView();
        }

        [HttpGet]
        public ActionResult Edit(int Id)
        {
            return PartialView(ProductDAL.Get(Id));
        }

        [HttpPost]
        public ActionResult Edit(ProductViewModel model)
        {
            if (ModelState.IsValid)
            {
                model.updateBy = User.Identity.Name;
                model.updateDate = DateTime.Now;
                if (ProductDAL.Koreksi(model.name, model.id) == true)
                {
                    if (ProductDAL.Update(model) == true)
                    {
                        var result = new
                        {
                            success = true,
                            alertType = "info",
                            alertStrong = "Data Update",
                            alertMessage = "Data Product has been update !"
                        };
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                }
                else if (ProductDAL.Koreksi(model.name, model.id) == false)
                {
                    var result = new { success = false };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            return PartialView();
        }


        public ActionResult Delete(int id)
        {
            ProductViewModel data = ProductDAL.Get(id);
            ProductDAL.Delete(id);
            var result = new
            {
                success = true
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

       
        [HttpGet]
        public ActionResult Detail(int id)
        {
            ProductViewModel data = ProductDAL.Get(id);
            return PartialView(data);
        }

        public JsonResult AmbilData(int id)
        {
            var result = new
            {
                success = true,
                data = ProductDAL.Get(id)
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }

    }
}