﻿using Marcomm.DAL;
using Marcomm.ViewModel;
using Marcommm.DAL;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Marcomm.Web.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        [AllowAnonymous]
        public ActionResult Login()
        {
            if (this.Request.IsAuthenticated)
            {
                return RedirectToAction("Index");
            }
            return PartialView();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = UserDAL.LoginCheck(model);
                if (user != null)
                {
                    SignInAsync(model.username);
                    UserViewModel item = UserDAL.GetByUsername(model.username);
                    Session["User"] = item;
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Invalid Login");
                }
            }
            return PartialView(model);
        }

        public ActionResult Logout()
        {
            var ctx = Request.GetOwinContext();
            var authenticationManager = ctx.Authentication;

            authenticationManager.SignOut();
            Session.Clear();
            Session.Abandon();

            return RedirectToAction("Login");
        }

        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return PartialView();
        }

        [HttpPost]
        [AllowAnonymous]
        public ActionResult ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                //kalo user ada maka update info user/ganti password
                var user = UserDAL.CheckUser(model);
                if (user != null)
                {
                    if (UserDAL.Update(user))
                    {
                        //setelah update otomatis login
                        SignInAsync(user.username);
                        return RedirectToAction("Index");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Invalid Login");
                }
            }
            return PartialView(model);
        }

        private void SignInAsync(string username)
        {
            var claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Name, username));
            var id = new ClaimsIdentity(claims, DefaultAuthenticationTypes.ApplicationCookie);

            var ctx = Request.GetOwinContext();
            var authenticationManager = ctx.Authentication;
            authenticationManager.SignIn(id);

        }

        // GET: User
        public ActionResult Index()
        {
            ViewBag.ListRole = new SelectList(RoleDAL.Get(), "name", "name");
            ViewBag.ListEmployee = new SelectList(EmployeeDAL.Get(), "fullname", "fullname");
            ViewBag.ListCompany = new SelectList(CompanyDAL.Get(), "name", "name");
            return View(UserDAL.Get());
        }

        public ActionResult List()
        {
            return PartialView(UserDAL.Get());
        }

        public ActionResult Add()
        {
            ViewBag.ListRole = new SelectList(RoleDAL.Get(), "id", "name");
            ViewBag.ListEmployee = new SelectList(EmployeeDAL.Get(), "id", "fullname");
            return PartialView();
        }

        [HttpPost]
        public ActionResult Add(UserViewModel model)
        {
            //model.createdBy = "Admin";
            model.isDelete = false;
            model.createdDate = DateTime.Now.Date;
            if (ModelState.IsValid)
            {
                if (UserDAL.CheckUser(model) == null)
                {
                    if (UserDAL.Insert(model))
                    {
                        var result = new { success = true, message = "Berhasil", action = "add", username = model.username };
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var result = new { success = false, message = "Gagal" };
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    var result = new { success = false, message = "The username " + model.username + " is already in use", exist = true };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                ViewBag.ListRole = new SelectList(RoleDAL.Get(), "id", "name");
                ViewBag.ListEmployee = new SelectList(EmployeeDAL.Get(), "id", "fullname");
            }

            return PartialView(model);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.ListRole = new SelectList(RoleDAL.Get(), "id", "name");
            ViewBag.ListEmployee = new SelectList(EmployeeDAL.Get(), "id", "fullname");
            return PartialView(UserDAL.Get(id));
        }

        [HttpPost]
        public ActionResult Edit(UserViewModel model)
        {
            if (UserDAL.Update(model))
            {
                var result = new { success = true, message = "Berhasil", action = "edit", username = model.username };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = new { success = false, message = "Gagal" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView();
        }

        public ActionResult Delete(int id)
        {
            return PartialView(UserDAL.Get(id));
        }

        [HttpPost]
        public ActionResult Delete(UserViewModel model)
        {
            if (UserDAL.Delete(model))
            {
                var result = new { success = true, message = "Berhasil", action = "delete", username = model.username };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = new { success = false, message = "Gagal" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView();
        }

        public ActionResult Detail(int id)
        {
            return PartialView(UserDAL.Get(id));
        }
    }
}