﻿using Marcomm.DAL;
using Marcomm.ViewModel;
using Marcommm.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Marcomm.Web.Controllers
{
    public class SouvenirController : Controller
    {
        // GET: Souvenir
        public ActionResult Index()
        {
            ViewBag.ListUnit = new SelectList(UnitDAL.Get(), "name", "name");
            return View(SouvenirDAL.Get());
        }

        public ActionResult List()
        {
            return PartialView(SouvenirDAL.Get());
        }

        public ActionResult Add()
        {
            var data = new SouvenirViewModel();
            data.code = SouvenirDAL.KodeAuto();
            ViewBag.ListUnit = new SelectList(UnitDAL.Get(), "id", "name");
            return PartialView(data);
        }

        [HttpPost]
        public ActionResult Add(SouvenirViewModel model)
        {
            ViewBag.ListUnit = new SelectList(UnitDAL.Get(), "id", "name");
            if (ModelState.IsValid)
            {
                if (SouvenirDAL.Insert(model))
                {
                    var result = new { success = true, message = "Data Berhasil Tersimpan" };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var result = new { success = false, message = "Data Gagal Tersimpan" };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            return PartialView();
        }

        public ActionResult Edit(int id)
        {
            ViewBag.ListUnit = new SelectList(UnitDAL.Get(), "id", "name");
            return PartialView(SouvenirDAL.Get(id));
        }

        [HttpPost]
        public ActionResult Edit(SouvenirViewModel model)
        {
            ViewBag.ListUnit = new SelectList(UnitDAL.Get(), "id", "name");
            //if (ModelState.IsValid)
            {
                if (SouvenirDAL.Update(model))
                {
                    var result = new { @success = true, message = "Success" };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var result = new { success = false, message = "Failed" };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
            }
            return PartialView(model);
        }

        public ActionResult Delete(int id)
        {
            return PartialView(SouvenirDAL.Get(id));
        }

        [HttpPost]
        public ActionResult Delete(SouvenirViewModel model)
        {
            if (SouvenirDAL.Delete(model))
            {
                var result = new { success = true, message = "Success" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = new { success = false, message = "Failed" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView();
        }

        public ActionResult Detail(int id)
        {
            return PartialView(SouvenirDAL.Get(id));
        }

        public JsonResult panggil(int id)
        {
            var result = new
            {
                data = CompanyDAL.Get(id),
                success = true
            };
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}