﻿using Marcomm.ViewModel;
using Marcomm.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Marcommm.DAL;

namespace Marcomm.Web.Controllers
{
    [Authorize]
    public class UnitController : Controller
    {
        public ActionResult Index()
        {
            return View(UnitDAL.Get());
        }

        public ActionResult List()
        {
            return PartialView(UnitDAL.Get());
        }

        /*[HttpPost]
        public ActionResult Search(UnitViewModel model)
        {
            return View("Index",UnitDAL.Get(model));
        }*/

        public ActionResult Add()
        {
            UnitViewModel Unit = new UnitViewModel();
            Unit.code = UnitDAL.getCode();

            return PartialView(Unit);
        }

        [HttpPost]
        public ActionResult Add(UnitViewModel model)
        {
            if (UnitDAL.cek(model.name) == false)
            {
                var User = (Marcomm.ViewModel.UserViewModel)Session["User"];
                model.isDelete = false;
                model.createdDate = DateTime.Now.Date;
                model.createdBy = User.mRoleName;
                if (ModelState.IsValid)
                {
                    if (UnitDAL.Insert(model))
                    {
                        var result = new { success = true, message = "Data Berhasil Tersimpan", code = model.code, from = "add" };
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var result = new { success = false, message = "Data Gagal Tersimpan" };
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                if (UnitDAL.cekAda(model.name) == true)
                {
                    var result = new { success = "ada", message = "Data sudah ada!", name = model.name };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    UnitDAL.UpdateAda(model);
                    var result = new { success = "kembali", name = model.name };
                    return Json(result, JsonRequestBehavior.AllowGet);

                }
            }
            return PartialView(model);
        }

        public ActionResult Edit(int id)
        {
            return PartialView(UnitDAL.Get(id));
        }

        [HttpPost]
        public ActionResult Edit(RoleViewModel model)
        {
            var User = (Marcomm.ViewModel.UserViewModel)Session["User"];
            model.updateBy = User.mRoleName;
            if (RoleDAL.Update(model))
            {
                var result = new { success = true, message = "Data Berhasil diubah", code = model.code, from = "edit" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = new { success = false, message = "Data Gagal diubah" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView();
        }

        public ActionResult Delete(int id)
        {
            return PartialView(UnitDAL.Get(id));
        }

        [HttpPost]
        public ActionResult Delete(UnitViewModel model)
        {
            if (UnitDAL.Delete(model))
            {
                var result = new { success = true, message = "Data Berhasil Dihapus", from = "delete", code = model.code };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = new { success = false, message = "Data Gagal Dihapus" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            return PartialView();
        }

        public ActionResult Details(int id)
        {
            return PartialView(UnitDAL.Get(id));
        }
    }
}