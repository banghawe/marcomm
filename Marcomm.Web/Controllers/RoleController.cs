﻿using Marcomm.ViewModel;
using Marcomm.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Marcommm.DAL;

namespace Marcomm.Web.Controllers
{
    public class RoleController : Controller
    {
        public ActionResult Index()
        {
            return View(RoleDAL.Get());
        }

        public ActionResult List()
        {
            return PartialView(RoleDAL.Get());
        }

        /*[HttpPost]
        public ActionResult Search(RoleViewModel model)
        {
            return View("Index",RoleDAL.Get(model));
        }*/

        public ActionResult Add()
        {
            RoleViewModel role = new RoleViewModel();
            role.code = RoleDAL.getCode();
            return PartialView(role);
        }

        [HttpPost]
        public ActionResult Add(RoleViewModel model)
        {
            if (RoleDAL.cek(model.name)==false)
            {
                var User = (Marcomm.ViewModel.UserViewModel)Session["User"];
                model.isDelete = false;
                model.createdDate = DateTime.Now.Date;
                model.createdBy = User.mRoleName;
                if (ModelState.IsValid)
                {
                    if (RoleDAL.Insert(model))
                    {
                        var result = new { success = true, message = "Data Berhasil Tersimpan", code = model.code, from = "add" };
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var result = new { success = false, message = "Data Gagal Tersimpan" };
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                }
            }else
            {
                if (RoleDAL.cekAda(model.name)==true)
                {
                    var result = new { success = "ada", message = "Data sudah ada!", name = model.name };
                    return Json(result, JsonRequestBehavior.AllowGet);
                }else
                {
                    RoleDAL.UpdateAda(model);
                    var result = new { success = "kembali", name = model.name };
                    return Json(result, JsonRequestBehavior.AllowGet);

                }
            }
            return PartialView(model);
        }

        public ActionResult Edit(int id)
        {
            return PartialView(RoleDAL.Get(id));
        }

        [HttpPost]
        public ActionResult Edit(RoleViewModel model)
        {
            var User = (Marcomm.ViewModel.UserViewModel)Session["User"];
            model.updateBy = User.mRoleName;
            if (RoleDAL.Update(model))
            {
                var result = new { success = true, message = "Data Berhasil diubah", code = model.code, from = "edit" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = new { success = false, message = "Data Gagal diubah" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            return PartialView();
        }

        public ActionResult Delete(int id)
        {
            return PartialView(RoleDAL.Get(id));
        }

        [HttpPost]
        public ActionResult Delete(RoleViewModel model)
        {
            if (RoleDAL.Delete(model))
            {
                var result = new { success = true, message = "Data Berhasil Dihapus", from="delete", code=model.code};
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var result = new { success = false, message = "Data Gagal Dihapus" };
                return Json(result, JsonRequestBehavior.AllowGet);
            }

            return PartialView();
        }

        public ActionResult Details(int id)
        {
            return PartialView(RoleDAL.Get(id));
        }
    }
}