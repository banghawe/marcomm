﻿using Marcomm.Model;
using Marcomm.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.DAL
{
    class DesignDAL
    {
        public static List<DesignViewModel> Get()
        {
            List<DesignViewModel> result = new List<DesignViewModel>();

            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblDesign
                          join x in db.tblEmployee on f.assignTo equals x.Id
                          where f.isDelete == false
                          select new DesignViewModel
                          {
                              id = f.id,
                              code = f.code,
                              tEventId = f.tEventId,
                              titleHeader = f.titleHeader,
                              requestBy = f.requestBy,
                              //requestDate = f.requestDate,
                              //approvedBy = f.approvedBy,
                              //approvedDate = f.approvedDate,
                              //assignTo = f.assignTo,
                              //closeDate = f.closeDate,
                              //note = f.note,
                              //status = f.status,
                              //rejectReason = f.rejectReason,
                              //isDelete = f.isDelete,
                              //createdBy = f.createdBy,
                              //createdDate = f.createdDate,
                              //updateBy = f.updateBy,
                              //updateDate = f.updateDate,
                              //staffName = x.fullName
                          }).ToList();
            }
            return result;
        }
    }
}
