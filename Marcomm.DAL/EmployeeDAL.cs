﻿using Marcomm.Model;
using Marcomm.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.DAL
{
    public class EmployeeDAL
    {
        public static List<EmployeeViewModel> Get()
        {
            List<EmployeeViewModel> result = new List<EmployeeViewModel>();
            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblEmployee
                          join x in db.tblCompany on f.mCompanyId equals x.id
                          where f.isDelete == false
                          select new EmployeeViewModel
                          {
                              Id = f.Id,
                              code = f.code,
                              firstName = f.firstName,
                              lastName = f.lastName,
                              mCompanyId = f.mCompanyId,
                              email = f.email,
                              createdBy = f.createdBy,
                              createdDate = f.createdDate,
                              updateBy = f.updateBy,
                              updateDate = f.updateDate,
                              fullName = f.firstName + " " + f.lastName,
                              companyName = x.name

                          }).ToList();
            }

            return result;
        }

        public static EmployeeViewModel Get(int Id)
        {
            EmployeeViewModel result = new EmployeeViewModel();
            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblEmployee
                          join x in db.tblCompany on f.mCompanyId equals x.id
                          where Id == f.Id
                          select new EmployeeViewModel
                          {
                              Id = f.Id,
                              code = f.code,
                              firstName = f.firstName,
                              lastName = f.lastName,
                              mCompanyId = f.mCompanyId,
                              email = f.email,
                              createdBy = f.createdBy,
                              createdDate = f.createdDate,
                              updateBy = f.updateBy,
                              updateDate = f.updateDate,
                              fullName = f.firstName + " " + f.lastName,
                              companyName = x.name
                          }).FirstOrDefault();
            }

            return result;
        }

        public static bool Update(EmployeeViewModel model)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                EmployeeModel item = db.tblEmployee.Find(model.Id);
                item.Id = model.Id;
                item.code = model.code;
                item.firstName = model.firstName;
                item.lastName = model.lastName;
                item.mCompanyId = model.mCompanyId;
                item.email = model.email;
                item.isDelete = model.isDelete;
                item.updateBy = "Admin";
                item.updateDate = DateTime.Now;


                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Insert(EmployeeViewModel model)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                EmployeeModel item = new EmployeeModel()
                {
                    Id = model.Id,
                    code = model.code,
                    firstName = model.firstName,
                    lastName = model.lastName,
                    mCompanyId = model.mCompanyId,
                    email = model.email,
                    isDelete = false,
                    createdBy = "Admin",
                    createdDate = DateTime.Now,
                    updateBy = "Admin",
                    updateDate = DateTime.Now

                };
                db.tblEmployee.Add(item);
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }

            return result;
        }

        public static bool Delete(int id)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                EmployeeModel item = db.tblEmployee.Find(id);
                item.isDelete = true;
                item.updateBy = "Admin";
                item.updateDate = DateTime.Now;
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {

                    result = false;
                }
            }

            return result;
        }

        public static bool NomorEmployee(string model, int id)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                var kode = db.tblEmployee.Where(x => x.code == model && x.Id != id).FirstOrDefault();
                if (kode != null)
                {
                    result = true;
                }
            }
            return result;
        }
    }

}
