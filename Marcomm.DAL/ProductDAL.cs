﻿using Marcomm.Model;
using Marcomm.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.DAL
{
    public class ProductDAL
    {
        public static List<ProductViewModel> Get()
        {
            List<ProductViewModel> result = new List<ProductViewModel>();

            using (MarcommContext db = new MarcommContext())
            {
                result = (from j in db.tblProduct
                          where j.isDelete == false
                          select new ProductViewModel
                          {
                              id = j.id,
                              code = j.code,
                              name = j.name,
                              description = j.description,
                              isDelete = j.isDelete,
                              createdBy = j.createdBy,
                              createdDate = j.createdDate,
                              updateBy = j.updateBy,
                              updateDate = j.updateDate
                          }).ToList();
            }
            return result;
        }

        public static ProductViewModel Get(int Id)
        {
            ProductViewModel result = new ProductViewModel();
            using (MarcommContext db = new MarcommContext())
            {
                result = (from j in db.tblProduct
                          where j.id == Id
                          select new ProductViewModel
                          {
                              id = j.id,
                              code = j.code,
                              name = j.name,
                              description = j.description,
                              isDelete = j.isDelete,
                              createdBy = j.createdBy,
                              createdDate = j.createdDate,
                              updateBy = j.updateBy,
                              updateDate = j.updateDate
                          }).FirstOrDefault();
            }

            return result;
        }

        public static bool Insert(ProductViewModel model)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                ProductModel item = new ProductModel()
                {
                    id = model.id,
                    code = model.code,
                    name = model.name,
                    description = model.description,
                    isDelete = false,
                    createdBy = model.createdBy,
                    createdDate = DateTime.Now,
                    updateBy = model.updateBy,
                    updateDate = DateTime.Now
                };
                db.tblProduct.Add(item);
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception e)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Update(ProductViewModel model)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                ProductModel item = db.tblProduct.Find(model.id);
                item.name = model.name;
                item.description = model.description;
                item.isDelete = false;
                item.updateBy = model.updateBy;
                item.updateDate = DateTime.Now;

                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception e)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(int id)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                ProductModel item = db.tblProduct.Find(id);
                item.isDelete = true;
                item.updateBy = "Admin";
                item.updateDate = DateTime.Now;
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static string KodeAuto()
        {
            string result = "";
            using (MarcommContext db = new MarcommContext())
            {
                var code = db.tblProduct.Select(x => new { Code = x.code }).OrderByDescending(x => x.Code).FirstOrDefault();
                if (code == null)
                {
                    result = "PR0001";
                }
                else
                {
                    var kd = code.Code;
                    var index = kd.Length;
                    var angka = kd.Substring(index - 4, 4);
                    var inc = int.Parse(angka) + 1;
                    result = "PR" + inc.ToString("0000");
                }
                return result;
            }
        }

        public static bool Koreksi(string nama, int id)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                var checknama = db.tblProduct.Where(x => x.name == nama && x.id != id).FirstOrDefault();
                if (checknama == null)
                {
                    result = true;
                }
            }
            return result;
        }

        public static bool Pulih(ProductViewModel model)
        {
            bool result = false;
            using(MarcommContext db = new MarcommContext())
            {
                var data = db.tblProduct.Where(x => x.name == model.name).FirstOrDefault();
                data.isDelete = false;
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }

            return result;
        }
    }
}
