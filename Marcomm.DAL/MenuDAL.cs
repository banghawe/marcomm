﻿using Marcomm.Model;
using Marcomm.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.DAL
{
    public class MenuDAL
    {
        public static List<MenuViewModel> Get()
        {
            List<MenuViewModel> result = new List<MenuViewModel>();

            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblMenu
                          where f.isDelete == false
                          select new MenuViewModel
                          {
                              Id = f.Id,
                              Code = f.Code,
                              name = f.name,
                              controller = f.controller,
                              parentId = f.parentId,
                              isDelete = f.isDelete,
                              createdBy = f.createdBy,
                              createdDate = f.createdDate
                          }).ToList();
            }
            return result;
        }

        public static MenuViewModel Get(int id)
        {
            MenuViewModel result = new MenuViewModel();

            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblMenu
                          where id == f.Id && f.isDelete == false
                          select new MenuViewModel
                          {
                              Id = f.Id,
                              Code = f.Code,
                              name = f.name,
                              controller = f.controller,
                              parentId = f.parentId,
                              isDelete = f.isDelete,
                              createdBy = f.createdBy,
                              createdDate = f.createdDate
                          }).FirstOrDefault();
            }
            return result;
        }

        public static bool Insert(MenuViewModel model)
        {
            bool result = false;

            using (MarcommContext db = new MarcommContext())
            {
                model.Code = CodeGenerator();
                MenuModel item = new MenuModel()
                {
                    Id = model.Id,
                    Code = model.Code,
                    name = model.name,
                    controller = model.controller,
                    parentId = model.parentId,
                    isDelete = false,
                    createdBy = model.createdBy,
                    createdDate = DateTime.Now,
                    updateDate = DateTime.Now
                };

                db.tblMenu.Add(item);


                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception e)
                {
                    result = false;
                }
            }

            return result;
        }

        public static bool Update(MenuViewModel model)
        {
            bool result = false;

            using (MarcommContext db = new MarcommContext())
            {
                MenuModel item = db.tblMenu.Find(model.Id);
                item.Id = model.Id;
                item.name = model.name;
                item.controller = model.controller;
                item.parentId = model.parentId;
                item.updateDate = DateTime.Now;
                item.isDelete = model.isDelete;
                item.updateBy = model.updateBy;

                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception e)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(MenuViewModel model)
        {
            bool result = false;

            using (MarcommContext db = new MarcommContext())
            {
                MenuModel item = db.tblMenu.Find(model.Id);
                item.Id = model.Id;
                item.isDelete = true;

                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception e)
                {
                    result = false;
                }
            }
            return result;
        }

        public static string CodeGenerator()
        {
            using (MarcommContext db = new MarcommContext())
            {
                string Code = "";
                //Code Auto Generator
                var urutan = db.tblMenu.OrderByDescending(x => x.Id).FirstOrDefault();
                if (urutan == null)
                {
                    Code = "ME0001";
                }
                else
                {
                    Code = "ME" + (Convert.ToInt32(urutan.Code.Substring(2, urutan.Code.Length - 2)) + 1).ToString("D4");
                }
                return Code;
            } 
        }

        public static bool CheckName(string name)
        {
            bool result = false;

            using (MarcommContext db = new MarcommContext())
            {
                MenuModel item = new MenuModel();

                item = db.tblMenu.Where(x => x.name == name).FirstOrDefault();
                if (item==null)
                {
                    result = true;
                }
                else
                {
                    result = false;
                }
            }

            return result;
        }

        public static MenuViewModel CheckIsDelete(MenuViewModel model)
        {
            MenuViewModel result = new MenuViewModel();

            using (MarcommContext db = new MarcommContext())
            {

                result = (from f in db.tblMenu
                            where f.name == model.name && f.isDelete == true
                            select new MenuViewModel{
                                Id = f.Id,
                                name = f.name,
                                controller = f.controller,
                                parentId = f.parentId,
                                createdDate = f.createdDate,
                                createdBy = f.createdBy,
                                isDelete = false,
                                updateDate = DateTime.Now
                            }).FirstOrDefault();

                if (result == null)
                {
                    return null;
                }
                else
                {
                    return result;
                }
            }
        }

        public static bool UpdateIsDelete(MenuViewModel model)
        {
            bool result = false;

            using (MarcommContext db = new MarcommContext())
            {
                MenuModel item = db.tblMenu.Find(model.Id);
                item.Id = model.Id;
                item.isDelete = false;

                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception e)
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
