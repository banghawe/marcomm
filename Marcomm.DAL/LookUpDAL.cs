﻿using Marcomm.Model;
using Marcomm.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.DAL
{
    public class LookUpDAL
    {
        public static List<LookUpViewModel> Get()
        {
            List<LookUpViewModel> result = new List<LookUpViewModel>();
            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblLookUp
                          select new LookUpViewModel
                          {
                              id = f.id,
                              name = f.name,
                              code = f.code,
                              groupby = f.groupby
                          }).ToList();
            }
            return result;
        }

        public static List<LookUpViewModel> GetYesNo()
        {
            List<LookUpViewModel> result = new List<LookUpViewModel>();
            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblLookUp
                          where f.groupby == "yesno"
                          select new LookUpViewModel
                          {
                              id = f.id,
                              name = f.name,
                              code = f.code,
                              groupby = f.groupby
                          }).ToList();
            }
            return result;
        }

        public static List<LookUpViewModel> GetToDo()
        {
            List<LookUpViewModel> result = new List<LookUpViewModel>();
            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblLookUp
                          where f.groupby == "todo"
                          select new LookUpViewModel
                          {
                              id = f.id,
                              name = f.name,
                              code = f.code,
                              groupby = f.groupby
                          }).ToList();
            }
            return result;
        }

        public static LookUpViewModel Get(int id)
        {
            LookUpViewModel result = new LookUpViewModel();
            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblLookUp
                          select new LookUpViewModel
                          {
                              id = f.id,
                              name = f.name,
                              code = f.code,
                              groupby = f.groupby
                          }).FirstOrDefault();
            }
            return result;
        }

        public static bool Insert(LookUpViewModel model)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                LookUpModel item = new LookUpModel()
                {
                    id = model.id,
                    code = model.code,
                    name = model.name,
                    groupby = model.groupby,
                };
                db.tblLookUp.Add(item);
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }
        
        public static bool Update(LookUpViewModel model)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                LookUpModel item = db.tblLookUp.Find(model.id);
                item.name = model.name;
                item.code = model.code;
                item.groupby = model.groupby;
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(LookUpViewModel model)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                LookUpModel item = db.tblLookUp.Find(model.id);
                db.tblLookUp.Remove(item);
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
