﻿using Marcomm.Model;
using Marcomm.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.DAL
{
    public class UserDAL
    {
        public static List<UserViewModel> Get()
        {
            List<UserViewModel> result = new List<UserViewModel>();

            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblUser
                          join x in db.tblRole on f.mRoleId equals x.id
                          join y in db.tblEmployee on f.mEmlpoyeeId equals y.Id
                          join z in db.tblCompany on y.mCompanyId equals z.id
                          where f.isDelete == false
                          select new UserViewModel
                          {
                              id = f.id,
                              username = f.username,
                              password = f.password,
                              mRoleId = f.mRoleId,
                              mRoleName = x.name,
                              mEmployeeId = f.mEmlpoyeeId,
                              mEmployeeName = y.firstName + " " + y.lastName,
                              mCompanyName = z.name,
                              isDelete = f.isDelete,
                              createdBy = f.createdBy,
                              createdDate = f.createdDate
                          }).ToList();
            }
            return result;
        }

        public static UserViewModel Get(int id)
        {
            UserViewModel result = new UserViewModel();

            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblUser
                          join x in db.tblRole on f.mRoleId equals x.id
                          join y in db.tblEmployee on f.mEmlpoyeeId equals y.Id
                          join z in db.tblCompany on y.mCompanyId equals z.id
                          where id == f.id && f.isDelete == false
                          select new UserViewModel
                          {
                              id = f.id,
                              username = f.username,
                              password = f.password,
                              mRoleId = f.mRoleId,
                              mRoleName = x.name,
                              mEmployeeId = f.mEmlpoyeeId,
                              mEmployeeName = y.firstName + " " + y.lastName,
                              mCompanyName = z.name,
                              isDelete = f.isDelete,
                              createdBy = f.createdBy,
                              createdDate = f.createdDate
                          }).FirstOrDefault();
            }
            return result;
        }

        public static List<UserViewModel> GetUser()
        {
            List<UserViewModel> result = new List<UserViewModel>();

            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblUser
                          join x in db.tblRole on f.mRoleId equals x.id
                          join y in db.tblEmployee on f.mEmlpoyeeId equals y.Id
                          join z in db.tblCompany on y.mCompanyId equals z.id
                          where f.isDelete == false && x.name == "Staff"
                          select new UserViewModel
                          {
                              id = f.id,
                              username = f.username,
                              password = f.password,
                              mRoleId = f.mRoleId,
                              mRoleName = x.name,
                              mEmployeeId = f.mEmlpoyeeId,
                              mEmployeeName = y.firstName + " " + y.lastName,
                              mCompanyName = z.name,
                              isDelete = f.isDelete,
                              createdBy = f.createdBy,
                              createdDate = f.createdDate
                          }).ToList();
            }
            return result;
        }

        public static bool Insert(UserViewModel model)
        {
            bool result = false;

            using (MarcommContext db = new MarcommContext())
            {
                UserModel item = new UserModel()
                {
                    id = model.id,
                    username = model.username,
                    password = model.password,
                    mRoleId = model.mRoleId,
                    mEmlpoyeeId = model.mEmployeeId,
                    isDelete = false,
                    createdBy = model.createdBy,
                    createdDate = DateTime.Now.Date,
                    updateDate = DateTime.Now
                };

                db.tblUser.Add(item);


                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception e)
                {
                    result = false;
                }
            }

            return result;
        }

        public static bool Update(UserViewModel model)
        {
            bool result = false;

            using (MarcommContext db = new MarcommContext())
            {
                UserModel item = db.tblUser.Find(model.id);
                item.id = model.id;
                item.username = model.username;
                item.password = model.password;
                item.mRoleId = model.mRoleId;
                item.mEmlpoyeeId = model.mEmployeeId;
                item.isDelete = false;
                item.createdBy = model.createdBy;
                item.updateDate = DateTime.Now;
                item.updateBy = model.updateBy;

                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception e)
                {
                    result = false;
                }
            }
            

            return result;
        }

        public static bool Delete(UserViewModel model)
        {
            bool result = false;

                using (MarcommContext db = new MarcommContext())
                {
                    UserModel item = db.tblUser.Find(model.id);
                    item.id = model.id;
                    item.isDelete = true;

                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception e)
                    {
                        result = false;
                    }
                }
            return result;
        }

        public static LoginViewModel LoginCheck(LoginViewModel model)
        {
            using(MarcommContext db = new MarcommContext())
            {
                LoginViewModel result = new LoginViewModel();

                result = (from f in db.tblUser
                          where f.username == model.username && f.password == model.password
                          select new LoginViewModel
                          {
                              username = f.username,
                              password = f.password
                          }).FirstOrDefault();

                if (result!=null)
                {
                    return result;
                }
                else
                {
                    return null;
                }
            }
        }

        public static UserViewModel CheckUser(ForgotPasswordViewModel model)
        {
            UserViewModel result = new UserViewModel();

            //check user ada atau tidak
            using(MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblUser
                          where f.username == model.username
                          select new UserViewModel
                          {
                              id = f.id,
                              username = f.username,
                              password = model.password
                          }).FirstOrDefault();
            }
            if (result!=null)
            {
                return result;
            }
            else
            {
                return null;
            }
            
        }

        public static UserViewModel CheckUser(UserViewModel model)
        {
            UserViewModel result = new UserViewModel();

            //check user ada atau tidak
            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblUser
                          where f.username == model.username && f.isDelete == false
                          select new UserViewModel
                          {
                              id = f.id,
                              username = f.username,
                              password = model.password
                          }).FirstOrDefault();
            }
            if (result != null)
            {
                return result;
            }
            else
            {
                return null;
            }

        }

        public static UserViewModel GetByUsername(string username)
        {
            UserViewModel result = new UserViewModel();
            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblUser
                          join x in db.tblRole on f.mRoleId equals x.id
                          join y in db.tblEmployee on f.mEmlpoyeeId equals y.Id
                          join z in db.tblCompany on y.mCompanyId equals z.id
                          where username == f.username && f.isDelete == false
                          select new UserViewModel
                          {
                              id = f.id,
                              username = f.username,
                              password = f.password,
                              mRoleId = f.mRoleId,
                              mRoleName = x.name,
                              mEmployeeId = f.mEmlpoyeeId,
                              mEmployeeName = y.firstName + " " + y.lastName,
                              mCompanyName = z.name,
                              isDelete = f.isDelete,
                              createdBy = f.createdBy,
                              createdDate = f.createdDate
                          }).FirstOrDefault();
            }

            return result;
        }
    }
}
