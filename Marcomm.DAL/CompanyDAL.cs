﻿using Marcomm.Model;
using Marcomm.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.DAL
{
    public class CompanyDAL
    {
        public static List<CompanyViewModel> Get()
        {
            List<CompanyViewModel> result = new List<CompanyViewModel>();

            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblCompany
                          where f.isDelete == false
                          select new CompanyViewModel
                          {
                              id = f.id,
                              code = f.code,
                              name = f.name,
                              address = f.address,
                              phone = f.phone,
                              email = f.email,
                              isDelete = f.isDelete,
                              createdBy = f.createdBy,
                              createdDate = f.createdDate,
                              updateBy = f.updateBy,
                              updateDate = f.updateDate
                          }).ToList();
            }
            return result;
        }

        public static CompanyViewModel Get(int id)
        {
            CompanyViewModel result = new CompanyViewModel();

            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblCompany
                          where id == f.id && f.isDelete == false
                          select new CompanyViewModel
                          {
                              id = f.id,
                              code = f.code,
                              name = f.name,
                              address = f.address,
                              phone = f.phone,
                              email = f.email,
                              isDelete = f.isDelete,
                              createdBy = f.createdBy,
                              createdDate = f.createdDate,
                              updateBy = f.updateBy,
                              updateDate = f.updateDate
                          }).FirstOrDefault();
            }
            return result;
        }

        public static bool Insert(CompanyViewModel model)
        {
            bool result = false;

            using (MarcommContext db = new MarcommContext())
            {
                CompanyModel item = new CompanyModel()
                {
                    id = model.id,
                    code = model.code,
                    name = model.name,
                    address = model.address,
                    phone = model.phone,
                    email = model.email,
                    isDelete = false,
                    createdBy = model.createdBy,
                    createdDate = DateTime.Now.Date,
                    updateBy = model.updateBy,
                    updateDate = DateTime.Now.Date
                };

                db.tblCompany.Add(item);

                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception e)
                {
                    result = false;
                }
            }

            return result;
        }

        public static bool Update(CompanyViewModel model)
        {
            bool result = false;

            using (MarcommContext db = new MarcommContext())
            {
                CompanyModel item = db.tblCompany.Find(model.id);
                item.id = model.id;
                item.code = model.code;
                item.name = model.name;
                item.address = model.address;
                item.phone = model.phone;
                item.email = model.email;
                item.isDelete = false;
                item.createdBy = model.createdBy;
                item.createdDate = DateTime.Now.Date;
                item.updateBy = model.updateBy;
                item.updateDate = DateTime.Now.Date;

                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception e)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(int id)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                CompanyModel item = db.tblCompany.Find(id);
                item.isDelete = true;
                item.updateBy = "Admin";
                item.updateDate = DateTime.Now;

                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }
        public static string KodeAuto()
        {
            string result = "";
            using (MarcommContext db = new MarcommContext())
            {
                var code = db.tblCompany.Select(x => new { Code = x.code }).OrderByDescending(x => x.Code).FirstOrDefault();
                if (code == null)
                {
                    result = "CP0001";
                }
                else
                {
                    var kd = code.Code;
                    var index = kd.Length;
                    var angka = kd.Substring(index - 4, 4);
                    var inc = int.Parse(angka) + 1;
                    result = "CP" + inc.ToString("0000");
                }
                return result;
            }
        }

        public static bool Cek(string nama, int id)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                var ceknama = db.tblCompany.Where(x => x.name == nama && x.id != id).FirstOrDefault();
                if (ceknama==null)
                {
                    result = true;
                }
            }
            return result;
        }
    }
}

