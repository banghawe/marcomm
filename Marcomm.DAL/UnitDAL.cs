﻿using Marcomm.Model;
using Marcomm.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.DAL
{
    public class UnitDAL
    {
        public static List<UnitViewModel> Get()
        {
            List<UnitViewModel> result = new List<UnitViewModel>();
            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblUnit
                          where f.isDelete == false
                          select new UnitViewModel
                          {
                              id = f.id,
                              code = f.code,
                              name = f.name,
                              description = f.description,
                              createdBy = f.createdBy,
                              createdDate = f.createdDate,
                              updateBy = f.updateBy,
                              updateDate = f.updateDate,
                          }).ToList();
            }
            return result;
        }

        public static List<UnitViewModel> Get(UnitViewModel model)
        {
            List<UnitViewModel> result = new List<UnitViewModel>();
            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblUnit
                          where f.isDelete == false || f.code.Contains(model.code)
                          || f.name.Contains(model.name)
                          || f.createdBy.Contains(model.createdBy)
                          || f.createdDate == model.createdDate
                          select new UnitViewModel
                          {
                              id = f.id,
                              code = f.code,
                              name = f.name,
                              description = f.description,
                              createdBy = f.createdBy,
                              createdDate = f.createdDate,
                              updateBy = f.updateBy,
                              updateDate = f.updateDate,
                          }).ToList();
            }
            return result;
        }

        public static UnitViewModel Get(int id)
        {
            UnitViewModel result = new UnitViewModel();
            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblUnit
                          where id == f.id
                          select new UnitViewModel
                          {
                              id = f.id,
                              code = f.code,
                              name = f.name,
                              description = f.description,
                              createdBy = f.createdBy,
                              createdDate = f.createdDate,
                              updateBy = f.updateBy,
                              updateDate = f.updateDate,
                          }).FirstOrDefault();
            }
            return result;
        }

        public static bool Insert(UnitViewModel model)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                UnitModel item = new UnitModel()
                {
                    id = model.id,
                    code = model.code,
                    name = model.name,
                    description = model.description,
                    isDelete = false,
                    createdBy = model.createdBy,
                    createdDate = DateTime.Now.Date
                };
                db.tblUnit.Add(item);
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static string getCode()
        {
            using (MarcommContext db = new MarcommContext())
            {
                string kode;
                var urutan = db.tblUnit.OrderByDescending(x => x.code).FirstOrDefault();
                if (urutan == null)
                {
                    kode = "UN001";
                }
                else
                {
                    kode = "UN" + (Convert.ToInt32(urutan.code.Substring(2, urutan.code.Length - 2)) + 1).ToString("D3");
                }
                return kode;
            }
        }
        public static bool Update(UnitViewModel model)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                UnitModel item = db.tblUnit.Find(model.id);
                item.name = model.name;
                item.description = model.description;
                item.updateBy = model.updateBy;
                item.updateDate = DateTime.Now.Date;
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(UnitViewModel model)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                UnitModel item = db.tblUnit.Find(model.id);
                item.isDelete = true;
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool cek(string nama)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                var data = db.tblUnit.Where(x => x.name == nama).FirstOrDefault();
                if (data == null)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
            return result;
        }

        public static bool cekAda(string nama)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                var data = db.tblUnit.Where(x => x.name == nama && x.isDelete == false).FirstOrDefault();
                if (data == null)
                {
                    result = false;
                }
                else
                {
                    result = true;
                }
            }
            return result;
        }
        public static bool UpdateAda(UnitViewModel model)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                var data = db.tblUnit.Where(x => x.name == model.name).FirstOrDefault();
                data.isDelete = false;
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
