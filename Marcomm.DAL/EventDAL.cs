﻿using Marcomm.Model;
using Marcomm.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.DAL
{
    public class EventDAL
    {
        public static List<EventViewModel> Get()
        {
            List<EventViewModel> result = new List<EventViewModel>();
            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblEvent
                          join x in db.tblLookUp on f.status equals x.code
                          join y in db.tblEmployee on f.requestBy equals y.Id
                          where f.isDelete == false && x.groupby == "Status Request"
                          select new EventViewModel
                          {
                              id = f.id,
                              code = f.code,
                              eventName = f.eventName,
                              place = f.place,
                              budget = f.budget,
                              rejectReason = f.rejectReason,
                              requestBy = f.requestBy,
                              request_date = f.request_date,
                              assignTo = f.assignTo,
                              createdBy = f.createdBy,
                              createdDate = f.createdDate,
                              updateBy = f.updateBy,
                              updateDate = f.updateDate,
                              status = f.status,
                              approvedBy = f.approvedBy,
                              approvedDate = f.approvedDate,
                              closeDate = f.closeDate,
                              isDelete = f.isDelete,
                              note = f.note,
                              namaStatus = x.name,
                              namaEmployee = y.firstName + " " + y.lastName,
                          }).ToList();
            }
            return result;
        }

        public static EventViewModel Get(int id)
        {
            EventViewModel result = new EventViewModel();
            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblEvent
                          join x in db.tblLookUp on f.status equals x.code
                          join y in db.tblEmployee on f.requestBy equals y.Id
                          where id == f.id && x.groupby == "Status Request"
                          select new EventViewModel
                          {
                              id = f.id,
                              code = f.code,
                              eventName = f.eventName,
                              place = f.place,
                              budget = f.budget,
                              rejectReason = f.rejectReason,
                              startDate = f.startDate,
                              endDate = f.endDate,
                              requestBy = f.requestBy,
                              request_date = f.request_date,
                              assignTo = f.assignTo,
                              createdBy = f.createdBy,
                              createdDate = f.createdDate,
                              updateBy = f.updateBy,
                              updateDate = f.updateDate,
                              status = f.status,
                              approvedBy = f.approvedBy,
                              approvedDate = f.approvedDate,
                              closeDate = f.closeDate,
                              isDelete = f.isDelete,
                              note = f.note,
                              namaStatus = x.name,
                              namaEmployee = y.firstName,
                          }).FirstOrDefault();
            }
            return result;
        }

        public static bool Insert(EventViewModel model)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                EventModel item = new EventModel()
                {
                    id = model.id,
                    code = model.code,
                    eventName = model.eventName,
                    startDate = model.startDate,
                    endDate = model.endDate,
                    place = model.place,
                    budget = model.budget,
                    requestBy = model.requestBy,
                    request_date = model.request_date,
                    createdBy = model.createdBy,
                    createdDate = model.createdDate,
                    isDelete = model.isDelete,
                    note = model.note,
                    status = model.status,
                };
                db.tblEvent.Add(item);
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static string getCode()
        {
            using (MarcommContext db = new MarcommContext())
            {
                string kode;
                var tgl = DateTime.Now.ToString();
                string[] dt = tgl.Split('/');
                string mm = dt[0].ToString();
                string dd = dt[1].ToString();
                string yy = dt[2].ToString();

                string thn = yy.Substring(2, 2);
                var urutan = db.tblEvent.Select(x => new { Code = x.code }).Where(x => x.Code.Contains(dd + mm + thn)).OrderByDescending(x => x.Code).FirstOrDefault();
                if (urutan == null)
                {
                    kode = "TRWOEV" + dd + mm + thn + "00001";
                }
                else
                {
                    kode = "TRWOEV" + dd + mm + thn + (Convert.ToInt32(urutan.Code.Substring(urutan.Code.Length - 5, 5)) + 1).ToString("D5");
                }
                return kode;
            }
        }
        public static bool Update(EventViewModel model)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                EventModel item = db.tblEvent.Find(model.id);
                item.eventName = model.eventName;
                item.place = model.place;
                item.startDate = model.startDate;
                item.endDate = model.endDate;
                item.budget = model.budget;
                item.note = model.note;
                item.updateBy = model.updateBy;
                item.updateDate = model.updateDate;
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool UpdateAppr(EventViewModel model)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                EventModel item = db.tblEvent.Find(model.id);
                item.assignTo = model.assignTo;
                item.approvedBy = model.approvedBy;
                item.approvedDate = model.approvedDate;
                item.status = model.status;
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool UpdateRjt(EventViewModel model)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                EventModel item = db.tblEvent.Find(model.id);
                item.status = model.status;
                item.rejectReason = model.rejectReason;
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }
        public static bool UpdateClose(EventViewModel model)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                EventModel item = db.tblEvent.Find(model.id);
                item.closeDate = model.closeDate;
                item.status = model.status;
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(EventViewModel model)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                EventModel item = db.tblEvent.Find(model.id);
                item.isDelete = true;
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }
        
        public static bool chkDate(DateTime? str, DateTime? end)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                if (str != null && end != null)
                {
                    int a = getDate(str);
                    int b = getDate(end);
                    if (a<=b)
                    {
                        result = true;
                    }else
                    {
                        result = false;
                    }
                }
            }
            return result;
        }

        public static int getDate(DateTime? date)
        {
            var tgl = date.ToString();
            string[] dt = tgl.Split('/');
            string mm = dt[0].ToString();  
            string dd = dt[1].ToString();
            string yy = dt[2].ToString();
            string thn = yy.Substring(0, 4);

            string Date = thn+mm+dd;
            int D = Convert.ToInt32(Date);
            return D;
        }
    }
}
