﻿using Marcomm.Model;
using Marcomm.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.DAL
{
    public class MenuAccessDAL
    {
        public static List<MenuAccessViewModel> Get()
        {
            List<MenuAccessViewModel> result = new List<MenuAccessViewModel>();
            using (MarcommContext db = new MarcommContext())
            {
                result = (from x in db.tblMenuAccess
                          join y in db.tblRole on x.mRoleId equals y.id
                          join z in db.tblMenu on x.mMenuId equals z.Id
                          where x.isDelete == false
                          select new MenuAccessViewModel
                          {
                              id = x.id,
                              mRoleId = x.mRoleId,
                              mMenuId = x.mMenuId,
                              isDelete = x.isDelete,
                              createdBy = x.createdBy,
                              updateBy = x.updateBy,
                              createdDate = x.createdDate,
                              updateDate = x.updateDate,
                              roleCode = y.code,
                              roleName = y.name,
                              menuAccess = z.name,
                          }).ToList();
            }
            return result;
        }
        public static MenuAccessViewModel Get(int roleid)
        {
            MenuAccessViewModel result = new MenuAccessViewModel();
            using (MarcommContext db = new MarcommContext())
            {
                result = (from x in db.tblMenuAccess
                          join y in db.tblRole on x.mRoleId equals y.id
                          join z in db.tblMenu on x.mMenuId equals z.Id
                          where x.mRoleId == roleid
                          select new MenuAccessViewModel
                          {
                              id = x.id,
                              mRoleId = x.mRoleId,
                              mMenuId = x.mMenuId,
                              isDelete = x.isDelete,
                              createdBy = x.createdBy,
                              updateBy = x.updateBy,
                              createdDate = x.createdDate,
                              updateDate = x.updateDate,
                              roleCode = y.code,
                              roleName = y.name,
                              menuAccess = z.name,
                          }).FirstOrDefault();
            }
            return result;
        }
        public static bool Insert(MenuAccessViewModel model)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                foreach (var menacc in model.ListMenuID)
                {
                    MenuAccessModel item = new MenuAccessModel()

                    {
                        id = model.id,
                        mRoleId = model.mRoleId,
                        mMenuId = menacc,
                        isDelete = false,
                        createdBy = model.createdBy,
                        createdDate = DateTime.Now,
                        updateDate = DateTime.Now
                    };
                    db.tblMenuAccess.Add(item);

                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {
                        result = false;
                        throw;
                    }
                }
            }
            return result;
        }
        public static bool Update(MenuAccessViewModel model)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                MenuAccessModel item = db.tblMenuAccess.Find(model.mRoleId);

                var data = db.tblMenuAccess.Where(x => x.mRoleId == model.mRoleId);
                db.tblMenuAccess.RemoveRange(data);
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {

                    throw;
                }
                foreach (var menacc in model.ListMenuID)
                {
                    MenuAccessModel dataadd = new MenuAccessModel()

                    {
                        mRoleId = model.mRoleId,
                        mMenuId = menacc,
                        isDelete = false,
                        createdBy = model.createdBy,
                        createdDate = DateTime.Now,
                        updateDate = DateTime.Now,
                        updateBy = model.updateBy
                    };
                    db.tblMenuAccess.Add(dataadd);

                    try
                    {
                        db.SaveChanges();
                        result = true;
                    }
                    catch (Exception)
                    {
                        result = false;
                        throw;
                    }
                }
            }
            return result;
        }

        public static bool Delete(MenuAccessViewModel model)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                MenuAccessModel item = db.tblMenuAccess.Find(model.mRoleId);
                var data = db.tblMenuAccess.Where(x => x.mRoleId == model.mRoleId);
                db.tblMenuAccess.RemoveRange(data);
                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static List<MenuAccessViewModel> getID(int id)
        {
            var data = new List<MenuAccessViewModel>();
            using (MarcommContext db = new MarcommContext())
            {
                data = db.tblMenuAccess.Where(x => x.mRoleId == id).Select(x => new MenuAccessViewModel()
                {
                    mMenuId = x.mMenuId

                }).ToList();
            }
            return data;
        }
    }
}
