﻿using Marcomm.Model;
using Marcomm.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.DAL
{
    public class SouvenirDAL
    {
        public static List<SouvenirViewModel> Get()
        {
            List<SouvenirViewModel> result = new List<SouvenirViewModel>();

            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblSouvenir
                          join x in db.tblUnit on f.mUnitId equals x.id
                          where f.isDelete == false
                          select new SouvenirViewModel
                          {
                              id = f.id,
                              code = f.code,
                              name = f.name,
                              description = f.description,
                              quantity = f.quantity,
                              mUnitId = f.mUnitId,
                              isDelete = f.isDelete,
                              createdBy = f.createdBy,
                              createdDate = f.createdDate,
                              updateBy = f.updateBy,
                              updateDate = f.updateDate,
                              unitName = x.name
                          }).ToList();
            }
            return result;
        }

        public static SouvenirViewModel Get(int id)
        {
            SouvenirViewModel result = new SouvenirViewModel();

            using (MarcommContext db = new MarcommContext())
            {
                result = (from f in db.tblSouvenir
                          join x in db.tblUnit on f.mUnitId equals x.id
                          where id == f.id && f.isDelete == false
                          select new SouvenirViewModel
                          {
                              id = f.id,
                              code = f.code,
                              name = f.name,
                              description = f.description,
                              quantity = f.quantity,
                              mUnitId = f.mUnitId,
                              isDelete = f.isDelete,
                              createdBy = f.createdBy,
                              createdDate = f.createdDate,
                              updateBy = f.updateBy,
                              updateDate = f.updateDate,
                              unitName = x.name
                          }).FirstOrDefault();
            }
            return result;
        }

        public static bool Insert(SouvenirViewModel model)
        {
            bool result = false;

            using (MarcommContext db = new MarcommContext())
            {
                SouvenirModel item = new SouvenirModel()
                {
                    id = model.id,
                    code = model.code,
                    name = model.name,
                    description = model.description,
                    quantity = model.quantity,
                    mUnitId = model.mUnitId,
                    isDelete = false,
                    createdBy = model.createdBy,
                    createdDate = DateTime.Now.Date,
                    updateBy = model.updateBy,
                    updateDate = DateTime.Now.Date
                };

                db.tblSouvenir.Add(item);

                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception e)
                {
                    result = false;
                }
            }

            return result;
        }

        public static bool Update(SouvenirViewModel model)
        {
            bool result = false;

            using (MarcommContext db = new MarcommContext())
            {
                SouvenirModel item = db.tblSouvenir.Find(model.id);
                item.id = model.id;
                item.code = model.code;
                item.name = model.name;
                item.description = model.description;
                item.quantity = model.quantity;
                item.mUnitId = model.mUnitId;
                item.isDelete = false;
                item.createdBy = model.createdBy;
                item.createdDate = DateTime.Now.Date;
                item.updateBy = model.updateBy;
                item.updateDate = DateTime.Now.Date;

                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception e)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(SouvenirViewModel model)
        {
            bool result = false;
            using (MarcommContext db = new MarcommContext())
            {
                SouvenirModel item = db.tblSouvenir.Find(model.id);
                item.isDelete = true;

                try
                {
                    db.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }

            return result;
        }

        public static string KodeAuto()
        {
            string result = "";
            using (MarcommContext db = new MarcommContext())
            {
                var code = db.tblSouvenir.Select(x => new { Code = x.code }).OrderByDescending(x => x.Code).FirstOrDefault();
                if (code == null)
                {
                    result = "SV0001";
                }
                else
                {
                    var kd = code.Code;
                    var index = kd.Length;
                    var angka = kd.Substring(index - 4, 4);
                    var inc = int.Parse(angka) + 1;
                    result = "SV" + inc.ToString("0000");
                }
                return result;
            }
        }
    }
}
