﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.ViewModel
{
    public class UserViewModel
    {
        [Display(Name = "Id")]
        public int id { get; set; }

        [Required]
        [Display(Name = "Username")]
        [MaxLength(50)]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [RegularExpression("^([0-9]+[a-zA-Z]+|[a-zA-Z]+[0-9]+)[0-9a-zA-Z]*$", ErrorMessage = "Please enter a valid username")]
        public String username { get; set; }

        [Required]
        [Display(Name = "Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [RegularExpression("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,50}$", ErrorMessage = "Please enter a valid password")]
        [DataType(DataType.Password)]
        public String password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("password", ErrorMessage = "The password and confirmation password do not match.")]
        public String ConfirmPassword { get; set; }

        [Required]
        [Display(Name = "Role ID")]
        public int mRoleId { get; set; }

        //NAME
        [Display(Name = "Role")]
        public String mRoleName { get; set; }

        [Required]
        [Display(Name = "Employee ID")]
        public int mEmployeeId { get; set; }

        //name
        [Display(Name = "Employee")]
        public String mEmployeeName { get; set; }

        [Display(Name = "Company")]
        public String mCompanyName { get; set; }

        [Required]
        [Display(Name = "is_delete")]
        public bool isDelete { get; set; }

        [Required]
        [Display(Name = "Created By")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Required]
        [Display(Name = "Created Date")]
        public DateTime createdDate { get; set; }

        [Display(Name = "Update By")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Display(Name = "Update Date")]
        public Nullable<DateTime> updateDate { get; set; }
    }
}
