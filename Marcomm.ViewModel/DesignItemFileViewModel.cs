﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.ViewModel
{
    public class DesignItemFileViewModel
    {
        [Display( Name = "id")]
        public int id { get; set; }

        [Required]
        [Display( Name = "t_design_item_id")]
        public int t_design_item_id { get; set; }

        [Display( Name = "filename")]
        [MaxLength(100)]
        public String filename { get; set; }

        [Display( Name = "size")]
        [MaxLength(11)]
        public String size { get; set; }

        [Display( Name = "extention")]
        [MaxLength(11)]
        public String extention { get; set; }

        [Required]
        [Display( Name = "is_delete")]
        public bool isDelete { get; set; }

        [Required]
        [Display( Name = "created_by")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Required]
        [Display( Name = "created_date")]
        public DateTime createdDate { get; set; }

        [Display( Name = "update_by")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Display( Name = "update_date")]
        public DateTime updateDate { get; set; }
    }
}
