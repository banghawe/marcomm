﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.ViewModel
{
    public class ProductViewModel
    {
        [Display(Name = "id")]
        public int id { get; set; }

        [Required]
        [Display(Name = "Product Code")]
        [MaxLength(50)]
        public String code { get; set; }

        [Required]
        [Display(Name = "Product Name")]
        [MaxLength(50)]
        public String name { get; set; }

        [Required]
        [Display(Name = "Description")]
        [MaxLength(255)]
        public String description { get; set; }

        [Display(Name = "is_delete")]
        public bool isDelete { get; set; }


        [Display(Name = "Created By")]
        [MaxLength(50)]
        public String createdBy { get; set; }


        [Display(Name = "Created Date")]

        public Nullable<DateTime> createdDate { get; set; }

        [Display(Name = "Update By")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Display(Name = "Update Date")]
        public Nullable<DateTime> updateDate { get; set; }
    }
}
