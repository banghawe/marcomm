﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.ViewModel
{
    public class PromotionItemViewModel
    {
        [Display( Name = "id")]
        public int id { get; set; }

        [Required]
        [Display( Name = "t_promotion_id")]
        public int tPromotionId { get; set; }

        [Display( Name = "t_design_item_id")]
        public int tDesignItemId { get; set; }

        [Required]
        [Display( Name = "m_product_id")]
        public int mProductId { get; set; }

        [Required]
        [Display( Name = "title")]
        [MaxLength(255)]
        public String title { get; set; }

        [Required]
        [Display( Name = "request_pic")]
        public int requestPic { get; set; }

        [Display( Name = "start_date")]
        public DateTime startDate { get; set; }

        [Display( Name = "end_date")]
        public DateTime endDate { get; set; }

        [Display( Name = "request_due_date")]
        public DateTime requestDueDate { get; set; }

        [Display( Name = "qty")]
        public long quality { get; set; }

        [Required]
        [Display( Name = "todo")]
        public int todo { get; set; }

        [Display( Name = "note")]
        [MaxLength(255)]
        public String note { get; set; }

        [Display( Name = "is_delete")]
        public bool isDelete { get; set; }

        [Required]
        [Display( Name = "created_by")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Required]
        [Display( Name = "created_date")]
        public DateTime createdDate { get; set; }

        [Display( Name = "update_by")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Display( Name = "update_date")]
        public DateTime updateDate { get; set; }
    }
}
