﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.ViewModel
{
    public class LookUpViewModel
    {
        [Column("id")]
        public int id { get; set; }

        [Column("groupby")]
        [MaxLength(50)]
        public String groupby { get; set; }

        [Column("name")]
        [MaxLength(255)]
        public String name { get; set; }

        [Column("code")]
        public int code { get; set; }
    }
}
