﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.ViewModel
{
    public class ForgotPasswordViewModel
    {
        [Required]
        [Display(Name = "Username")]
        [MaxLength(50)]
        public String username { get; set; }

        [Required]
        [Display(Name = "New Password")]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        public String password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Retype New Password")]
        [Compare("password", ErrorMessage = "The password and confirmation password do not match.")]
        public String ConfirmPassword { get; set; }
    }
}
