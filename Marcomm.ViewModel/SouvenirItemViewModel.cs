﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.ViewModel
{
    public class SouvenirItemViewModel
    {
        [Display( Name = "id")]
        public int id { get; set; }

        [Required]
        [Display( Name = "t_souvenir_id")]
        public int tSouvenirId { get; set; }

        [Required]
        [Display( Name = "m_souvenir_id")]
        public int mSouvenirId { get; set; }

        [Display( Name = "qty")]
        public long qty { get; set; }

        [Display( Name = "qty_settlement")]
        public long qtySettlement { get; set; }

        [Display( Name = "note")]
        [MaxLength(255)]
        public String note { get; set; }

        [Display( Name = "is_delete")]
        public bool isDelete { get; set; }

        [Required]
        [Display( Name = "created_by")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Required]
        [Display( Name = "created_date")]
        public DateTime createdDate { get; set; }

        [Display( Name = "update_by")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Display( Name = "update_date")]
        public DateTime updateDate { get; set; }
    }
}
