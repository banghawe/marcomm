﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.ViewModel
{
    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Username")]
        [MaxLength(50)]
        public String username { get; set; }

        [Required]
        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        public String password { get; set; }
    }
}
