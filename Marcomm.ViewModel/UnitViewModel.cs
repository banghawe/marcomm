﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.ViewModel
{
    public class UnitViewModel
    {
        [Display(Name = "id")]
        public int id { get; set; }

        [Required]
        [Display(Name = "code")]
        [MaxLength(50)]
        public String code { get; set; }

        [Required]
        [Display(Name = "name")]
        [MaxLength(50)]
        public String name { get; set; }

        [Display(Name = "description")]
        [MaxLength(255)]
        public String description { get; set; }

        [Display(Name = "is_delete")]
        public bool isDelete { get; set; }

        [Display(Name = "created_by")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Display(Name = "created_date")]
        public Nullable<DateTime> createdDate { get; set; }

        [Display(Name = "update_by")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Display(Name = "update_date")]
        public Nullable<DateTime> updateDate { get; set; }
    }
}
