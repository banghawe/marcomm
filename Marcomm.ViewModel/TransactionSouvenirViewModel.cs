﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.ViewModel
{
    public class TransactionSouvenirViewModel
    {
        [Display( Name = "id")]
        public int id { get; set; }

        [Required]
        [Display( Name = "code")]
        [MaxLength(50)]
        public String code { get; set; }

        [Required]
        [Display( Name = "type")]
        [MaxLength(11)]
        public String type { get; set; }

        [Display( Name = "t_event_id")]
        public int tEventId { get; set; }

        [Required]
        [Display( Name = "request_by")]
        public int requestBy { get; set; }

        [Display( Name = "request_date")]
        public DateTime requestDate { get; set; }

        [Display( Name = "request_due_date")]
        public DateTime requestDueDate { get; set; }

        [Display( Name = "approved_by")]
        public int approvedBy { get; set; }

        [Display( Name = "approved_date")]
        public DateTime approvedDate { get; set; }

        [Display( Name = "received_by")]
        public int receivedBy { get; set; }

        [Display( Name = "received_date")]
        public DateTime receivedDate { get; set; }

        [Display( Name = "settlement_by")]
        public int settlementBy { get; set; }

        [Display( Name = "settlement_date")]
        public DateTime settlementDate { get; set; }

        [Display( Name = "settlement_approved_by")]
        public int settlementApprovedBy { get; set; }

        [Display( Name = "settlement_approved_date")]
        public DateTime settlementApprovedDate { get; set; }

        [Display( Name = "status")]
        public int Status { get; set; }

        [Display( Name = "note")]
        [MaxLength(255)]
        public String note { get; set; }

        [Display( Name = "reject_reason")]
        [MaxLength(255)]
        public String rejectReason { get; set; }

        [Display( Name = "is_delete")]
        public bool isDelete { get; set; }

        [Required]
        [Display( Name = "created_by")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Required]
        [Display( Name = "created_date")]
        public DateTime createdDate { get; set; }

        [Display( Name = "update_by")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Display( Name = "update_date")]
        public DateTime updateDate { get; set; }
    }
}
