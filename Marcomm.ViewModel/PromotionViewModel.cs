﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.ViewModel
{
    public class PromotionViewModel
    {
        [Display( Name = "id")]
        public int id { get; set; }

        [Required]
        [Display( Name = "code")]
        [MaxLength(50)]
        public String code { get; set; }

        [Required]
        [Display( Name = "flag_design")]
        [MaxLength(1)]
        public char flagDesign { get; set; }

        [Required]
        [Display( Name = "title")]
        [MaxLength(255)]
        public String title { get; set; }

        [Required]
        [Display( Name = "t_event_id")]
        public int tEventId { get; set; }

        [Display( Name = "t_design_id")]
        public int tDesignId { get; set; }

        [Display( Name = "request_by")]
        public int requestBy { get; set; }

        [Required]
        [Display( Name = "request_date")]
        public DateTime request_date { get; set; }

        [Display( Name = "approved_by")]
        public int approvedBy { get; set; }

        [Display( Name = "approved_date")]
        public DateTime approvedDate { get; set; }

        [Display( Name = "assign_to")]
        public int assignTo { get; set; }


        [Display( Name = "closed_date")]
        public DateTime closeDate { get; set; }

        [Display( Name = "note")]
        [MaxLength(255)]
        public String note { get; set; }

        [Display( Name = "status")]
        [MaxLength(11)]
        public int status { get; set; }

        [Display( Name = "reject_reason")]
        [MaxLength(255)]
        public String rejectReason { get; set; }

        [Display( Name = "is_delete")]
        public bool isDelete { get; set; }

        [Required]
        [Display( Name = "created_by")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Required]
        [Display( Name = "created_date")]
        public DateTime createdDate { get; set; }

        [Display( Name = "update_by")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Display( Name = "update_date")]
        public DateTime updateDate { get; set; }
    }
}
