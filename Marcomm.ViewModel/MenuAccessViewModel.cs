﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.ViewModel
{
    public class MenuAccessViewModel
    {
        [Display(Name = "id")]
        public int id { get; set; }

        [Required]
        [Display(Name = "m_role_id")]
        public int mRoleId { get; set; }


        [Display(Name = "m_menu_id")]
        public int mMenuId { get; set; }

        [Display(Name = "is_delete")]
        public bool isDelete { get; set; }

        [Display(Name = "created_by")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Display(Name = "created_date")]
        public DateTime createdDate { get; set; }

        [Display(Name = "update_by")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Display(Name = "update_date")]
        public DateTime updateDate { get; set; }

        [Display(Name = "role_code")]
        public String roleCode { get; set; }


        [Display(Name = "role_name")]
        public string roleName { get; set; }

        [Display(Name = "menu_access")]
        public string menuAccess { get; set; }

        public List<int> ListMenuID { get; set; }

        public RoleViewModel Role { get; set; }
    }
}
