﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.ViewModel
{
    public class EventViewModel
    {
        [Display(Name = "id")]
        public int id { get; set; }

        [Required]
        [Display(Name = "code")]
        [MaxLength(50)]
        public String code { get; set; }

        [Required]
        [Display(Name = "Event Name")]
        [MaxLength(255)]
        public String eventName { get; set; }

        [Required]
        [Display(Name = "Start Date")]
        public DateTime? startDate { get; set; }

        [Required]
        [Display(Name = "End Date")]
        public DateTime? endDate { get; set; }

        [Required]
        [Display(Name = "Place")]
        [MaxLength(255)]
        public String place { get; set; }

        [RegularExpression("^[0-9]+$", ErrorMessage ="Number Only!")]
        [Display(Name = "Budget")]
        public Nullable<long> budget { get; set; }

        [Required]
        [Display(Name = "request_by")]
        public int requestBy { get; set; }

        [Display(Name = "request_date")]
        public Nullable<DateTime> request_date { get; set; }

        [Display(Name = "approved_by")]
        public int approvedBy { get; set; }

        [Display(Name = "approved_date")]
        public Nullable<DateTime> approvedDate { get; set; }

        [Display(Name = "assign_to")]
        public int assignTo { get; set; }


        [Display(Name = "closed_date")]
        public Nullable<DateTime> closeDate { get; set; }

        [Display(Name = "note")]
        [MaxLength(255)]
        public String note { get; set; }

        [Display(Name = "status")]
        public int status { get; set; }

        [Display(Name = "reject_reason")]
        [MaxLength(255)]
        public String rejectReason { get; set; }

        [Display(Name = "is_delete")]
        public bool isDelete { get; set; }

        [Display(Name = "created_by")]
        public String createdBy { get; set; }

        [Display(Name = "created_date")]
        public Nullable<DateTime> createdDate { get; set; }

        [Display(Name = "update_by")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Display(Name = "update_date")]
        public Nullable<DateTime> updateDate { get; set; }

        public string namaStatus { get; set; }
        public string namaEmployee { get; set; }
    }
}
