﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.ViewModel
{
    public class CompanyViewModel
    {
        [Display(Name = "Id")]
        public int id { get; set; }

        [Required]
        [Display(Name = "Company Code")]
        [MaxLength(50)]
        public String code { get; set; }

        [Required]
        [Display(Name = "Company Name")]
        [MaxLength(50)]
        public String name { get; set; }

        [Display(Name = "Address")]
        [MaxLength(255)]
        public String address { get; set; }

        [Display(Name = "Phone")]
        [Phone]
        [MaxLength(50)]
        [RegularExpression("^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\\/0-9]*$",ErrorMessage="Phone Number is not valid")]
        public String phone { get; set; }

        [Display(Name = "Email")]
        [EmailAddress]
        [MaxLength(50)]
        public String email { get; set; }

        [Required]
        [Display(Name = "is_delete")]
        public bool isDelete { get; set; }

        [Required]
        [Display(Name = "Created By")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Required]
        [Display(Name = "Created Date")]
        //[DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode=true, DataFormatString="{0:MM/dd/yyyy}")]
        public DateTime createdDate { get; set; }

        [Display(Name = "Update By")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Display(Name = "Update Date")]
        public DateTime updateDate { get; set; }
    }
}
