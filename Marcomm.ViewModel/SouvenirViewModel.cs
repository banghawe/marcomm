﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.ViewModel
{
    public class SouvenirViewModel
    {
        [Display(Name = "id")]
        public int id { get; set; }

        [Required]
        [Display(Name = "Souvenir Code")]
        [MaxLength(50)]
        public String code { get; set; }

        [Required]
        [Display(Name = "Souvenir Name")]
        [MaxLength(50)]
        public String name { get; set; }

        [Display(Name = "Description")]
        [MaxLength(255)]
        public String description { get; set; }

        //[Required]
        [Display(Name = "quantitys")]
        public int quantity { get; set; }

        [Required]
        [Display(Name = "m_unit_id")]
        public int mUnitId { get; set; }

        [Display(Name = "Unit Name")]
        public String mUnitName { get; set; }

        [Required]
        [Display(Name = "is_delete")]
        public bool isDelete { get; set; }

        [Required]
        [Display(Name = "Created By")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Required]
        [Display(Name = "Created Date")]
        public DateTime createdDate { get; set; }

        [Display(Name = "update_by")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Display(Name = "update_date")]
        public DateTime updateDate { get; set; }

        public string unitName { get; set; }
        public UnitViewModel Unit { get; set; }
    }
}
