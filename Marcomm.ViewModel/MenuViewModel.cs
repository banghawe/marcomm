﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.ViewModel
{
    public class MenuViewModel
    {
        [Display(Name = "id")]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Code")]
        [MaxLength(50)]
        public String Code { get; set; }

        [Required]
        [Display(Name = "Menu Name")]
        [MaxLength(50)]
        [RegularExpression("^[A-Za-z]+$", ErrorMessage="The name is not valid, accept only alphabet")]
        public String name { get; set; }

        [Required]
        [Display(Name = "Controller Name")]
        [MaxLength(50)]
        public String controller { get; set; }

        [Display(Name = "Parent id")]
        [RegularExpression("^[0-9]+$", ErrorMessage = "The parent id is not valid, accept only numeric")]
        public Nullable<int> parentId { get; set; }

        [Display(Name = "Parent")]
        public int parentName { get; set; }

        [Required]
        [Display(Name = "is_delete")]
        public bool isDelete { get; set; }

        [Required]
        [Display(Name = "Created By")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Required]
        [Display(Name = "Created Date")]
        public DateTime createdDate { get; set; }

        [Display(Name = "Updated By")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Display(Name = "Updated Date")]
        public DateTime updateDate { get; set; }
    }
}
