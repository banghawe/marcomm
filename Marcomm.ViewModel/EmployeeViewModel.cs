﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.ViewModel
{
    public class EmployeeViewModel
    {
        [Display(Name = "Id")]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Employee ID Number")]
        [MaxLength(50)]
        public String code { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [MaxLength(50)]
        public String firstName { get; set; }

        [Display(Name = "Last Name")]
        [MaxLength(50)]
        public String lastName { get; set; }

        [Display(Name = "Company ID")]
        public int mCompanyId { get; set; }

        [Display(Name = "Company Name")]
        public String mCompanyName { get; set; }

        [Display(Name = "Email")]
        [MaxLength(150)]
        [EmailAddress]
        public String email { get; set; }

        [Display(Name = "is_delete")]
        public bool isDelete { get; set; }

        [Display(Name = "Created By")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Display(Name = "Created Date")]
        [DataType(DataType.DateTime)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public Nullable<DateTime> createdDate { get; set; }

        [Display(Name = "Updated By")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Display(Name = "Update Date")]
        public Nullable<DateTime> updateDate { get; set; }

        public string fullName { get; set; }
        public string companyName { get; set; }
        public CompanyViewModel Company { get; set; }
    }
}
