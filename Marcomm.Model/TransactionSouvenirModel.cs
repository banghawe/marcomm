﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.Model
{
    [Table("t_souvenir")]
    public class TransactionSouvenirModel
    {
        [Key]
        [Column("id")]
        public int id { get; set; }

        [Required]
        [Column("code")]
        [MaxLength(50)]
        public String code { get; set; }

        [Required]
        [Column("type")]
        [MaxLength(11)]
        public String type { get; set; }

        [Column("t_event_id")]
        public int tEventId { get; set; }

        [Required]
        [Column("request_by")]
        public int requestBy { get; set; }

        [Column("request_date")]
        public DateTime requestDate { get; set; }

        [Column("request_due_date")]
        public DateTime requestDueDate { get; set; }

        [Column("approved_by")]
        public int approvedBy { get; set; }

        [Column("approved_date")]
        public DateTime approvedDate { get; set; }

        [Column("received_by")]
        public int receivedBy { get; set; }

        [Column("received_date")]
        public DateTime receivedDate { get; set; }

        [Column("settlement_by")]
        public int settlementBy { get; set; }

        [Column("settlement_date")]
        public DateTime settlementDate { get; set; }

        [Column("settlement_approved_by")]
        public int settlementApprovedBy { get; set; }

        [Column("settlement_approved_date")]
        public DateTime settlementApprovedDate { get; set; }

        [Column("status")]
        public int Status { get; set; }

        [Column("note")]
        [MaxLength(255)]
        public String note { get; set; }

        [Column("reject_reason")]
        [MaxLength(255)]
        public String rejectReason { get; set; }

        [Column("is_delete")]
        public bool isDelete { get; set; }

        [Required]
        [Column("created_by")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Required]
        [Column("created_date")]
        public DateTime createdDate { get; set; }

        [Column("update_by")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Column("update_date")]
        public DateTime updateDate { get; set; }


    }
}
