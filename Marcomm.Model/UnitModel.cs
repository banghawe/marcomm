﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.Model
{
    [Table("m_unit")]
    public class UnitModel
    {
        [Key]
        [Column("id")]
        public int id { get; set; }

        [Required]
        [Column("code")]
        [MaxLength(50)]
        public String code { get; set; }

        [Required]
        [Column("name")]
        [MaxLength(50)]
        public String name { get; set; }

        [Column("description")]
        [MaxLength(255)]
        public String description { get; set; }

        [Required]
        [Column("is_delete")]
        public bool isDelete { get; set; }

        [Required]
        [Column("created_by")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Required]
        [Column("created_date")]
        public Nullable<DateTime> createdDate { get; set; }

        [Column("update_by")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Column("update_date")]
        public Nullable<DateTime> updateDate { get; set; }
    }
}
