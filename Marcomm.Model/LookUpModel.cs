﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.Model
{
    [Table("t_lookup")]
    public class LookUpModel
    {
        [Key]
        [Column("id")]
        public int id { get; set; }

        [Required]
        [Column("groupby")]
        [MaxLength(50)]
        public String groupby { get; set; }

        [Required]
        [Column("name")]
        [MaxLength(255)]
        public String name { get; set; }

        [Required]
        [Column("code")]
        public int code { get; set; }
    }
}
