﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.Model
{
    [Table("t_event")]
    public class EventModel
    {
        [Key]
        [Column("id")]
        public int id { get; set; }

        [Required]
        [Column("code")]
        [MaxLength(50)]
        public String code { get; set; }

        [Required]
        [Column("event_name")]
        [MaxLength(255)]
        public String eventName { get; set; }

        [Column("start_date")]
        public Nullable<DateTime> startDate { get; set; }

        [Column("end_date")]
        public Nullable<DateTime> endDate { get; set; }

        [Column("place")]
        [MaxLength(255)]
        public String place { get; set; }

        [Column("budget")]
        public Nullable<long> budget { get; set; }

        [Required]
        [Column("request_by")]
        public int requestBy { get; set; }

        [Required]
        [Column("request_date")]
        public Nullable<DateTime> request_date { get; set; }

        [Column("approved_by")]
        public int approvedBy { get; set; }

        [Column("approved_date")]
        public Nullable<DateTime> approvedDate { get; set; }

        [Column("assign_to")]
        public int assignTo { get; set; }

        [Column("closed_date")]
        public Nullable<DateTime> closeDate { get; set; }

        [Column("note")]
        [MaxLength(255)]
        public String note { get; set; }

        [Column("status")]
        public int status { get; set; }

        [Column("reject_reason")]
        [MaxLength(255)]
        public String rejectReason { get; set; }

        [Column("is_delete")]
        public bool isDelete { get; set; }

        [Column("created_by")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Column("created_date")]
        public Nullable<DateTime> createdDate { get; set; }

        [Column("update_by")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Column("update_date")]
        public Nullable<DateTime> updateDate { get; set; }
    }
}
