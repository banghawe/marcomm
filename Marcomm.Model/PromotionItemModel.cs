﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.Model
{
    [Table("t_promotion_item")]
    public class PromotionItemModel
    {
        [Key]
        [Column("id")]
        public int id { get; set; }

        [Required]
        [Column("t_promotion_id")]
        public int tPromotionId { get; set; }

        [Column("t_design_item_id")]
        public int tDesignItemId { get; set; }

        [Required]
        [Column("m_product_id")]
        public int mProductId { get; set; }

        [Required]
        [Column("title")]
        [MaxLength(255)]
        public String title { get; set; }

        [Required]
        [Column("request_pic")]
        public int requestPic { get; set; }

        [Column("start_date")]
        public DateTime startDate { get; set; }

        [Column("end_date")]
        public DateTime endDate { get; set; }

        [Column("request_due_date")]
        public DateTime requestDueDate { get; set; }

        [Column("qty")]
        public long quality { get; set; }

        [Required]
        [Column("todo")]
        public int todo { get; set; }

        [Column("note")]
        [MaxLength(255)]
        public String note { get; set; }

        [Column("is_delete")]
        public bool isDelete { get; set; }

        [Required]
        [Column("created_by")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Required]
        [Column("created_date")]
        public DateTime createdDate { get; set; }

        [Column("update_by")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Column("update_date")]
        public DateTime updateDate { get; set; }
    }
}
