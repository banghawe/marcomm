﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.Model
{
    public class MarcommContext:DbContext
    {
        public MarcommContext() : base("MarcommConnection")
        {

        }
        public DbSet<LookUpModel> tblLookUp { get; set; }
        public DbSet<RoleModel> tblRole { get; set; }
        public DbSet<EmployeeModel> tblEmployee { get; set; }
        public DbSet<UserModel> tblUser { get; set; }
        public DbSet<MenuModel> tblMenu { get; set; }
        public DbSet<CompanyModel> tblCompany { get; set; }
        public DbSet<MenuAccessModel> tblMenuAccess { get; set; }
        public DbSet<UnitModel> tblUnit { get; set; }
        public DbSet<SouvenirModel> tblSouvenir { get; set; }
        public DbSet<ProductModel> tblProduct { get; set; }
        public DbSet<EventModel> tblEvent { get; set; }
        public DbSet<DesignModel> tblDesign { get; set; }
        public DbSet<DesignItemModel> tblDesignItem { get; set; }
        public DbSet<DesignItemFileModel> tblDesignItemFile { get; set; }
        public DbSet<PromotionModel> tblPromotion { get; set; }
        public DbSet<PromotionItemModel> tblPromotionItem { get; set; }
        public DbSet<PromotionItemFileModel> tblPromotionItemFile { get; set; }
        public DbSet<TransactionSouvenirModel> tblTransactionSouvenirModel { get; set; }
        public DbSet<SouvenirItemModel> tblSouvenirItemModel { get; set; }
    }
}
