﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.Model
{
    [Table("souvenir_item")]
    public class SouvenirItemModel
    {
        [Key]
        [Column("id")]
        public int id { get; set; }

        [Required]
        [Column("t_souvenir_id")]
        public int tSouvenirId { get; set; }

        [Required]
        [Column("m_souvenir_id")]
        public int mSouvenirId { get; set; }

        [Column("qty")]
        public long qty { get; set; }

        [Column("qty_settlement")]
        public long qtySettlement { get; set; }

        [Column("note")]
        [MaxLength(255)]
        public String note { get; set; }

        [Column("is_delete")]
        public bool isDelete { get; set; }

        [Required]
        [Column("created_by")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Required]
        [Column("created_date")]
        public DateTime createdDate { get; set; }

        [Column("update_by")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Column("update_date")]
        public DateTime updateDate { get; set; }
    }
}
