﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.Model
{
    [Table("t_promotion_item_file")]
    public class PromotionItemFileModel
    {
        [Key]
        [Column("id")]
        public int id { get; set; }

        [Required]
        [Column("t_promotion_id")]
        public int tPromotionId { get; set; }

        [Column("filename")]
        [MaxLength(255)]
        public String filename { get; set; }

        [Column("size")]
        [MaxLength(11)]
        public String size { get; set; }

        [Column("extention")]
        [MaxLength(11)]
        public String extention { get; set; }

        [Column("start_date")]
        public DateTime startDate { get; set; }

        [Column("end_date")]
        public DateTime endDate { get; set; }

        [Column("request_due_date")]
        public DateTime requestDueDate { get; set; }

        [Column("qty")]
        public long quality { get; set; }

        [Required]
        [Column("todo")]
        public int todo { get; set; }

        [Column("note")]
        [MaxLength(255)]
        public String note { get; set; }

        [Column("is_delete")]
        public bool isDelete { get; set; }

        [Required]
        [Column("created_by")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Required]
        [Column("created_date")]
        public DateTime createdDate { get; set; }

        [Column("update_by")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Column("update_date")]
        public DateTime updateDate { get; set; }
    }
}
