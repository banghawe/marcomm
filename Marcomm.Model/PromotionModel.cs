﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.Model
{
    [Table("t_promotion")]
    public class PromotionModel
    {
        [Key]
        [Column("id")]
        public int id { get; set; }

        [Required]
        [Column("code")]
        [MaxLength(50)]
        public String code { get; set; }

        [Required]
        [Column("flag_design")]
        [MaxLength(1)]
        public char flagDesign { get; set; }

        [Required]
        [Column("title")]
        [MaxLength(255)]
        public String title { get; set; }

        [Required]
        [Column("t_event_id")]
        public int tEventId { get; set; }

        [Column("t_design_id")]
        public int tDesignId { get; set; }

        [Column("request_by")]
        public int requestBy { get; set; }

        [Required]
        [Column("request_date")]
        public DateTime request_date { get; set; }

        [Column("approved_by")]
        public int approvedBy { get; set; }

        [Column("approved_date")]
        public DateTime approvedDate { get; set; }

        [Column("assign_to")]
        public int assignTo { get; set; }


        [Column("closed_date")]
        public DateTime closeDate { get; set; }

        [Column("note")]
        [MaxLength(255)]
        public String note { get; set; }

        [Column("status")]
        public int status { get; set; }

        [Column("reject_reason")]
        [MaxLength(255)]
        public String rejectReason { get; set; }

        [Column("is_delete")]
        public bool isDelete { get; set; }

        [Required]
        [Column("created_by")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Required]
        [Column("created_date")]
        public DateTime createdDate { get; set; }

        [Column("update_by")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Column("update_date")]
        public DateTime updateDate { get; set; }


    }
}
