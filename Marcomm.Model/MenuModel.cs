﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.Model
{
    [Table("m_menu")]
    public class MenuModel
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [Column("code")]
        [MaxLength(50)]
        public String Code { get; set; }

        [Required]
        [Column("name")]
        [MaxLength(50)]
        public String name { get; set; }

        [Required]
        [Column("controller")]
        [MaxLength(50)]
        public String controller { get; set; }

        [Column("parent_id")]
        public Nullable<int> parentId { get; set; }

        [Required]
        [Column("is_delete")]
        public bool isDelete { get; set; }

        [Required]
        [Column("created_by")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Required]
        [Column("created_date")]
        public DateTime createdDate { get; set; }

        [Column("update_by")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Column("update_date")]
        public DateTime updateDate { get; set; }
    }
}
