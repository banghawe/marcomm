﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.Model
{
    [Table("t_design_item_file")]
    public class DesignItemFileModel
    {
        [Key]
        [Column("id")]
        public int id { get; set; }

        [Required]
        [Column("t_design_item_id")]
        public int t_design_item_id { get; set; }

        [Column("filename")]
        [MaxLength(100)]
        public String filename { get; set; }

        [Column("size")]
        [MaxLength(11)]
        public String size { get; set; }

        [Column("extention")]
        [MaxLength(11)]
        public String extention { get; set; }

        [Required]
        [Column("is_delete")]
        public bool isDelete { get; set; }

        [Required]
        [Column("created_by")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Required]
        [Column("created_date")]
        public DateTime createdDate { get; set; }

        [Column("update_by")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Column("update_date")]
        public DateTime updateDate { get; set; }
    }
}
