﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.Model
{
    [Table("m_user")]
    public class UserModel
    {
        [Key]
        [Column("id")]
        public int id { get; set; }

        [Required]
        [Column("username")]
        [MaxLength(50)]
        public String username { get; set; }

        [Required]
        [Column("password")]
        [MaxLength(50)]
        public String password { get; set; }

        [Required]
        [Column("m_role_id")]
        public int mRoleId { get; set; }

        [Required]
        [Column("m_employee_id")]
        public int mEmlpoyeeId { get; set; }

        [Required]
        [Column("is_delete")]
        public bool isDelete { get; set; }

        [Required]
        [Column("created_by")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Required]
        [Column("created_date")]
        public DateTime createdDate { get; set; }

        [Column("update_by")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Column("update_date")]
        public DateTime updateDate { get; set; }
    }
}
