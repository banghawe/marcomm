﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Marcomm.Model
{
    [Table("m_employee")]
    public class EmployeeModel
    {
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [Column("employee_number")]
        [MaxLength(50)]
        public String code { get; set; }

        [Required]
        [Column("first_name")]
        [MaxLength(50)]
        public String firstName { get; set; }

        [Column("last_name")]
        [MaxLength(50)]
        public String lastName { get; set; }

        [Column("m_company_id")]
        public int mCompanyId { get; set; }

        [Column("email")]
        [MaxLength(150)]
        public String email { get; set; }

        [Required]
        [Column("is_delete")]
        public bool isDelete { get; set; }

        [Required]
        [Column("created_by")]
        [MaxLength(50)]
        public String createdBy { get; set; }

        [Required]
        [Column("created_date")]
        public DateTime createdDate { get; set; }

        [Column("update_by")]
        [MaxLength(50)]
        public String updateBy { get; set; }

        [Column("update_date")]
        public Nullable<DateTime> updateDate { get; set; }

    }
}
